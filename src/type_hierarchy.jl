# ===========================================================
#  Copyright (C) 2022 Gerda Wyssen, Daniel Fitze, Matthias Ertl, University of Bern,
#  gerda.wyssen@unibe.ch, daniel.fitze@unibe.ch, matthias.ertl@unibe.ch
# 
#  This file is part of mpc.
# 
#  mpc is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
# 
#  mpc is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with mpc.  If not, see <http://www.gnu.org/licenses/>.
#  ===========================================================


# connection and server stuff
abstract type LAB_CONFIG end
abstract type SESSION_TYPE end
abstract type POSITION end
abstract type INPUT end

# OUTPUT
abstract type OUTPUT end
abstract type display end

## stimulus stuff
#abstract type event end
abstract type EVENT end
abstract type motion end
abstract type trigger end
#abstract type kernel end
#
## glw stuff
#abstract type glw_scene end
#abstract type screen_type end
#abstract type world_object end
#abstract type primitive <: world_object end
#abstract type virtual_screen <: world_object end
#abstract type dimension end
#abstract type color end
#abstract type seq_step end
#abstract type media end
#
## gvs stuff
#abstract type stimulator end
#
## effe stuff
#abstract type effe_type end
#abstract type effe_motion end





