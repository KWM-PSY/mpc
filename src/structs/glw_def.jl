# ===========================================================
#  Copyright (C) 2022 Gerda Wyssen, Daniel Fitze and Matthias Ertl, University of Bern,
#  matthias.ertl@unibe.ch
#  gerda.wyssen@unibe.ch 
#  daniel.fitze@unibe.ch # 
#
#  This file is part of mpc.
# 
#  mpc is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
# 
#  mpc is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with mpc. If not, see <http://www.gnu.org/licenses/>.
#  ===========================================================
@kwdef struct glworld
  displays::Array{screen_type} 
  dev_state::Int64 = 0
  objects::Array{world_object} = []
end

@kwdef struct vr <: screen_type 
  name::String = "vr1"
  type::String = "pimax"
  motion_tracking::Int64 = 0
end

@kwdef struct screen <: screen_type 
  name::String = "pc1"
  type::String = "screen"
  position::position = mpc.position2d(0, 0)
  orientation::orientation = mpc.orientation() 
  resolution::resolution = mpc.resolution()
  motion_tracking::Int64 = 0
end

@kwdef struct flat <: virtual_screen 
  name::String = "f1"
  resolution::resolution = mpc.resolution(1000, 1000)
  dimension::dimension = mpc.dimension2d(6.0, 4.5)
  position::position = mpc.position3d(0.0, 0.0, 0.0)
  orientation::orientation = mpc.orientation() 
  image::Int64 = 1
  movie::Int64 = 0
end

@kwdef struct sphere <: virtual_screen 
  name::String = "s1"
  position::position = mpc.position3d(0.0, 0.0, 0.0)
  orientation::orientation = mpc.orientation() 
  resolution::resolution = mpc.resolution(1024, 768)
  radius::Float64 = 2
  project::String = "inner"
end

@kwdef struct light_source <: world_object
  name::String = "default"
  position::position = mpc.position3d(0.0, 0.0, 5.0)
  radius::Float64 = -1.0 
  color::color = mpc.white()
end

@kwdef struct camera <: world_object
  screen_name::String = "w1"
  position::position = mpc.position3d(4.0, 4.0, 4.0)
  direction::position = mpc.position3d(0.0, 0.0, 0.0)
  rotation::Float64 = deg2rad(180.0)
  fov::Float64 = deg2rad(90)
  near::Float64 = 0.2
  far::Float64 = 200.0
end

@kwdef mutable struct object_position
  name::String = "default"
  position::position = mpc.position3d()
  orientation::orientation = mpc.orientation()
end





