# ===========================================================
#  Copyright (C) 2022 Gerda Wyssen, Daniel Fitze and Matthias Ertl, University of Bern,
#  matthias.ertl@unibe.ch
#  gerda.wyssen@unibe.ch 
#  daniel.fitze@unibe.ch # 
#
#  This file is part of mpc.
# 
#  mpc is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
# 
#  mpc is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with mpc. If not, see <http://www.gnu.org/licenses/>.
#  ===========================================================
@kwdef struct neurocon <: stimulator
  device::Int = 1
  ports::Array{Int} = [0]
  lower::Array{Float64} = [0.0]
  upper::Array{Float64} = [2.0]
  srate::Float64 = 60.0

end

@kwdef struct virtual <: stimulator
  device::Int = 1
  ports::Array{Int} = [0]
  lower::Array{Float64} = [0.0]
  upper::Array{Float64} = [2.0]
  srate::Float64 = 60.0
end

@kwdef struct gvs_profile
  chan::Int = 1
  name::String = ""
  lower::Int = 0
  upper::Int = 4095
  interrupt::Int = 0
  repetitions::Int = 0
end




