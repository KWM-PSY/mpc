# ===========================================================
#  Copyright (C) 2022 Gerda Wyssen, Daniel Fitze, Matthias Ertl, University of Bern,
#  gerda.wyssen@unibe.ch, daniel.fitze@unibe.ch, matthias.ertl@unibe.ch
# 
#  This file is part of mpc.
# 
#  mpc is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
# 
#  mpc is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with mpc.  If not, see <http://www.gnu.org/licenses/>.
#  ===========================================================
@kwdef mutable struct input_state
    gamepad_state::Union{Int, Nothing} = nothing
    listen_to_btn::Bool = false
    wait_for_btn::Union{Bool, Nothing} = nothing
    btn_pressed::Union{Bool, Nothing} = nothing
    btn::Union{Vector{String}, Nothing} = nothing
    btn_time::Union{Vector{Int}, Nothing} = nothing
    likert_state::Union{likert, Nothing} = nothing
    ipd_state::Union{ipd_setting, Nothing} = nothing
end

@kwdef mutable struct output_state
    motion_state::Union{Int, Nothing} = nothing
    motion_start::Union{Int, Nothing} = nothing
    motion_end::Union{Int, Nothing} = nothing
    audio_state::Union{Int, Nothing} = nothing
    img_state::Union{Int, String, Nothing} = nothing
    transducer_state::Union{Int, Nothing} = nothing
end

@kwdef mutable struct state
    udp_port#::mpc.Port
    topic::Union{String, Nothing} = nothing
    session_tag::Union{String, Nothing} = nothing

    master_state::Union{Int, Nothing} = nothing
    srv_state::Union{Int, Nothing} = nothing
    pltf_state::Union{Int, Nothing} = nothing
    current_dof::pltf_position = pltf_position()

    login_state::Union{Int, Nothing} = nothing
    engage_state::Union{Int, Nothing} = nothing
    logout_state::Union{Int, Nothing} = nothing
    park_state::Union{Int, Nothing} = nothing

    input_state::input_state = mpc.input_state()
    output_state::output_state = mpc.output_state()

    err::Array{Union{String, Int},2} = ["msg" "err"]

    stop_loop::Bool = false
end


