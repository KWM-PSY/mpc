# ===========================================================
#  Copyright (C) 2022 Gerda Wyssen, Daniel Fitze, Matthias Ertl, University of Bern,
#  gerda.wyssen@unibe.ch, daniel.fitze@unibe.ch, matthias.ertl@unibe.ch
# 
#  This file is part of mpc.
# 
#  mpc is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
# 
#  mpc is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with mpc.  If not, see <http://www.gnu.org/licenses/>.
#  ===========================================================
"""
    pltf_position(lateral, heave, surge, yaw, pitch, roll)

Defines either one or a series of positions on all degrees of freedom.

Lateral, heave and surge are in mm Yaw, pitch and roll are radians. For a series of positions it is ensured that they match in length.
"""
@kwdef mutable struct pltf_position
    lateral::Union{Vector{Float64}, Float64} = 0.0
    heave::Union{Vector{Float64}, Float64} = 0.0
    surge::Union{Vector{Float64}, Float64} = 0.0
    yaw::Union{Vector{Float64}, Float64} = 0.0
    pitch::Union{Vector{Float64}, Float64} = 0.0
    roll::Union{Vector{Float64}, Float64} = 0.0
    pltf_position(l,h,s,y,p,r) = length(l) == length(h) == length(s) == length(y) == length(p) == length(r) ? new(l,h,s,y,p,r) : error("length of dofs must match!")
end

