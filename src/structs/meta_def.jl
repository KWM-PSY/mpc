# ===========================================================
#  Copyright (C) 2022 Gerda Wyssen, Daniel Fitze and Matthias Ertl, University of Bern,
#  matthias.ertl@unibe.ch
#  gerda.wyssen@unibe.ch 
#  daniel.fitze@unibe.ch # 
#
#  This file is part of mpc.
# 
#  mpc is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
# 
#  mpc is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with mpc. If not, see <http://www.gnu.org/licenses/>.
#  ===========================================================

struct meta_data
  exp_ID::String
  exp_date::String
  exp_time::String

  subject_ID::String
  subject_Sex::String
  subject_Age::String
  subject_Hand::String

  log_filename::String

  meta_data(exp_ID,
            exp_date = string(Dates.format(Dates.now(), "yyyy-mm-dd")),
            exp_time = string(Dates.format(Dates.now(), "HH-MM-SS")),
            subject_ID = string(get_input("SubjectID", "string", [])),
            subject_Sex = string(get_input("Sex", "string", ["m";"w";"o"])),
            subject_Age = string(get_input("Age", "Int", [18;120])),
	    subject_Hand = string(get_input("Handedness", "string", ["r";"l";"a"])),
            log_filename = (exp_ID * "_" * subject_ID * "_" * exp_date * "_" * exp_time)) = new(exp_ID, 
												exp_date, 
												exp_time, 
												subject_ID, 
												subject_Sex, 
												subject_Age, 
												subject_Hand, 
												log_filename)
end


