abstract type EVENT_LOG end

@kwdef mutable struct img_log <: EVENT_LOG
    event::Union{Tuple{String,Float64}, Nothing} = nothing  
    name::Union{String, Nothing} = nothing
end

@kwdef mutable struct audio_log <: EVENT_LOG
    event::Union{Tuple{String,Float64}, Nothing} = nothing  
    name::Union{String, Nothing} = nothing
end

@kwdef mutable struct input_log <: EVENT_LOG
    event::Union{Tuple{String,Float64}, Nothing} = nothing  
    btn_pressed::Union{Bool, Nothing} = nothing
    btn::Union{Array{String}, Nothing} = nothing
    btn_time::Union{Array{Int}, Nothing} = nothing
    likert_pos::Union{Int, Nothing} = nothing
end

@kwdef mutable struct motion_log <: EVENT_LOG
    event::Union{Tuple{String,Float64}, Nothing} = nothing  
    motion_start::Union{Int,Nothing} = nothing
    motion_end::Union{Int, Nothing} = nothing
end
    
@kwdef mutable struct trial_log
    motion_log::Union{motion_log, Nothing} = nothing
    img_log::Union{img_log, Nothing} = nothing
    audio_log::Union{audio_log, Nothing} = nothing
    input_log::Union{likert, Nothing} = nothing
end
