"""
|param    | MotionLab 1      | MotionLab 2   |
|:------  |:----------------:|:-------------:|
|pltf:    | "Moog_1"         | "Moog_2       |
|client:  | "192.168.1.212"  | "10.33.1.50"  |
|srv:     | "192.168.1.200"  | "10.33.1.1"   |
"""
@kwdef mutable struct config
    rc::LAB_CONFIG = mpc.unibe_d269()
    input::input = mpc.input()
    output::output = mpc.output()
end

@kwdef mutable struct config_special
    rc::LAB_CONFIG = mpc.unibe_d169()
end
