# ===========================================================
#  Copyright (C) 2022 Gerda Wyssen, Daniel Fitze and Matthias Ertl, University of Bern,
#  matthias.ertl@unibe.ch
#  gerda.wyssen@unibe.ch 
#  daniel.fitze@unibe.ch # 
#
#  This file is part of mpc.
# 
#  mpc is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
# 
#  mpc is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with mpc. If not, see <http://www.gnu.org/licenses/>.
#  ===========================================================
@kwdef struct orientation 
  yaw::Float64 = 0
  pitch::Float64 = 0
  roll::Float64 = 0
end

@kwdef struct dimension2d <: dimension 
  x::Float64 = 0
  y::Float64 = 0
end

@kwdef struct dimension3d <: dimension 
  x::Float64 = 0
  y::Float64 = 0
  z::Float64 = 0
end

@kwdef struct resolution 
  width::Int64 = 1280 
  height::Int64 = 1024
end

@kwdef struct white <: color
  r::Int64 = 255 
  g::Int64 = 255
  b::Int64 = 255
end

@kwdef struct red <: color
  r::Int64 = 255 
  g::Int64 = 1
  b::Int64 = 1
end

@kwdef struct def_color <: color
  r::Int64 = 125 
  g::Int64 = 125
  b::Int64 = 125
end

@kwdef struct def_hexcolor <: color
  r::String = "55"  
  g::String = "55" 
  b::String = "55" 
end



