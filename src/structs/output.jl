"""
provide parameters needed to open the respective server
if nothing it wont be opened
"""
@kwdef mutable struct output
    motion::Union{SESSION_TYPE, Nothing} = nothing
    audio::Union{audio_srv, Nothing} = nothing
    img::Union{img_srv, Nothing} = nothing
    transducer::Union{transducer_srv, Nothing} = nothing
end
