@kwdef mutable struct likert_resp <: EVENT
    empty_screen::img_stim = mpc.img_stim(screen = "ALL",
                                     name = "NONE",
                                     exit_on = "ack")
    imgs_names::Vector{String} = ["slider_1", "slider_2", "slider_3", "slider_4", "slider_5", "slider_6", "slider_7"]

    imgs::Union{Vector{img_stim}, Nothing} = nothing
    log::Bool = true
end


