# ===========================================================
#  Copyright (C) 2022 Gerda Wyssen, Daniel Fitze, Matthias Ertl, University of Bern,
#  gerda.wyssen@unibe.ch, daniel.fitze@unibe.ch, matthias.ertl@unibe.ch
# 
#  This file is part of mpc.
# 
#  mpc is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
# 
#  mpc is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with mpc.  If not, see <http://www.gnu.org/licenses/>.
#  ===========================================================
@kwdef struct framed <: DISPLAY
    name::String ="window"
    top_left_x_coord::Int = 0
    top_left_y_coord::Int = 0
    upper_margin::Int = 640
    lower_margin::Int = 480
    type::String = "FRAMED"
end
@kwdef struct fullscreen <: DISPLAY
    name::String = "fullscreen"
    left_margin::Int = 60
    right_margin::Int = 60
    upper_margin::Int = 60
    lower_margin::Int = 60
    type::String = "FULLSCREEN" # fullscreen (pc), VISOR_...
end
@kwdef struct img_srv
    img_dir::String = "old"
    displays::Vector{DISPLAY} 
end

@kwdef struct img_stim <: EVENT
    screen::String = "ALL"
    name::String = "b15"
    exit_on::String = "ack"
end
