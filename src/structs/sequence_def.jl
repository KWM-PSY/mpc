# ===========================================================
#  Copyright (C) 2021 Gerda Wyssen, Daniel Fitze and Matthias Ertl, University of Bern,
#  matthias.ertl@unibe.ch
#  gerda.wyssen@unibe.ch 
#  daniel.fitze@unibe.ch 
#
#  This file is part of mpc.
# 
#  mpc is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
# 
#  mpc is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with mpc. If not, see <http://www.gnu.org/licenses/>.
#  ===========================================================

@kwdef struct sequence 
  name::String = "Sequence 1"
  duration::Float64 = 5.0
  nb_object::Int = 1
end

@kwdef struct displacement <: seq_step
  name::String = "displacement 1"
  object_id::Int = 1
  start_time::Float64 = 1.0
  end_time::Float64 = 3.0
  abs_or_rel::String = "A"
  time_interp::String = "LINEAR"
  spatial_interp::String = "LIN"
  position::Array{position3d} = [mpc.position3d()]
end

@kwdef struct rotation <: seq_step
  name::String = "rotation 1"
  object_id::Int = 1
  start_time::Float64 = 1.0
  end_time::Float64 = 3.0
  abs_or_rel::String = "A"
  time_interp::String = "LINEAR"
  spatial_interp::String = "LIN"
  position::Array{orientation} = [mpc.orientation()]
end

@kwdef struct set_visible <: seq_step
  name::String = "visible 1"
  object_id::Int = 1
  start_time::Float64 = 1.0
  end_time::Float64 = 3.0
  visible::Int = 1
end

@kwdef struct set_camera <: seq_step
  name::String = "c 1"
  object_id::Int = 1
  start_time::Float64 = 1.0
  end_time::Float64 = 3.0
  camera::camera = mpc.camera()  
end

@kwdef struct set_image <: media
  name::String = "S"
  object_id::Int = 1
  start_time::Float64 = 1.0
  end_time::Float64 = 3.0
  file::String = "" 
end

@kwdef struct set_movie <: media
  name::String = "M"
  object_id::Int = 1
  start_time::Float64 = 1.0
  end_time::Float64 = 3.0
  file::String = "" 
end


