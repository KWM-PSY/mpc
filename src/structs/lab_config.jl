@kwdef mutable struct unibe_d169 <: LAB_CONFIG
    pltf_model::String = "Yaw_2"
    sampling_freq::Int = 200
    max_acc_allowed::Int = 200
    client_ip::String = "10.34.1.50" 
    server_ip::String = "10.34.1.1"
    server_outport::Int = 1860
    server_inport::Int = 1966
    server_user::String = "placo"
    path_libmoogskt::String = "/home/mertl/GIT/MOO-c-interface/c/libmoogskt2.so"
    path_data::String = "../data/"
    log_resolution::Int = 100
    mailto::String = ""
    disp_info::Bool = true
    topic::String = "selfmotion"
end

@kwdef mutable struct unibe_d269 <: LAB_CONFIG
    pltf_model::String = "Moog_2"
    sampling_freq::Int = 200
    max_acc_allowed::Int = 200
    client_ip::String = "10.33.1.50" 
    server_ip::String = "10.33.1.1"
    server_outport::Int = 1860
    server_inport::Int = 1966
    server_user::String = "placo"
    path_libmoogskt::String = "/opt/GIT/MOO-c-interface/c/libmoogskt2.so"
    path_data::String = "../data/"
    log_resolution::Int = 100
    mailto::String = ""
    disp_info::Bool = true
    topic::String = "master"
end

@kwdef mutable struct unibe_d369 <: LAB_CONFIG
    pltf_model::String = "Moog_1"
    sampling_freq::Int = 60
    max_acc_allowed::Float64 = 0.03
    client_ip::String = "192.168.1.212" 
    server_ip::String = "192.168.1.200"
    server_outport::Int = 1860
    server_inport::Int = 1966
    server_user::String = "placo"
    path_libmoogskt::String = "/home/kwm/julia/MOO-c-interface/c/libmoogskt2.so"
    path_data::String = "../data/"
    log_resolution::Int = 100
    mailto::String = ""
    disp_info::Bool = true
    topic::String = "master"
end

@kwdef mutable struct unibe_d369new <: LAB_CONFIG
    pltf_model::String = "Moog_1"
    sampling_freq::Int = 60
    max_acc_allowed::Int = 300
    client_ip::String = "10.35.1.50" 
    server_ip::String = "10.35.1.1"
    server_outport::Int = 1860
    server_inport::Int = 1966
    server_user::String = "placo"
    path_libmoogskt::String = "/home/kwm/julia/MOO-c-interface/c/libmoogskt2.so"
    path_data::String = "../data/"
    log_resolution::Int = 100
    mailto::String = ""
    disp_info::Bool = true
    topic::String = "master"
end

@kwdef mutable struct rochester <: LAB_CONFIG
    pltf_model::String = "Moog_1"
    sampling_freq::Int = 60
    max_acc_allowed::Int = 0.2
    client_ip::String = "x.x.x.x" 
    server_ip::String = "x.x.x.x"
    server_outport::Int = 1860
    server_inport::Int = 1966
    path_libmoogskt::String = "/adf/asdf"
    path_data::String = "../data/"
    log_resolution::Int = 100
    mailto::String = ""
    disp_info::Bool = true
    topic::String = "master"
end

