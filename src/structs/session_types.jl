#  ===========================================================
#  Copyright (C) 2021 Matthias Ertl, Gerda Wyssen, Daniel Fitze, University of Bern,
#  matthias.ertl@unibe.ch
#  gerda.wyssen@unibe.ch 
#  daniel.fitze@unibe.ch # 
#
#  This file is part of mpc.
# 
#  mpc is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
# 
#  mpc is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with mpc. If not, see <http://www.gnu.org/licenses/>.
#  ===========================================================
@kwdef mutable struct immediate <: SESSION_TYPE
  start_height::Float64 = 670
  max_diff_lin::Float64
  max_diff_rot::Float64
end

@kwdef mutable struct immediate_vel <: SESSION_TYPE
  start_height::Float64 = 670.0
  max_diff_lin::Float64 = 0.0
  max_diff_rot::Float64 = deg2rad(1.0)
end

@kwdef mutable struct short <: SESSION_TYPE
    actuator_mode::String = "D"
    start_height::Float64 = 670
end

@kwdef mutable struct long <: SESSION_TYPE
  actuator_mode::String = "D"
  start_height::Float64 = 670
end

@kwdef mutable struct joystick <: SESSION_TYPE
  max_actuator_vel::Float64 = 0.15
  rotcenter_heave::POSITION = mpc.position3d(0.0, 0.0, 0.0)
end

@kwdef mutable struct feedback <: SESSION_TYPE
  active_joystick::String = "feedback"
  axes_spec::String = "#p|x|-0.174533|0.174533#p|z|-0.174533|0.174533"
  delay::Float64 = 0.0
  smoothing_type::String = "E#"
  smoothing_duration::Float64 = 0.01
  buttons::Array{String} = []
end

@kwdef mutable struct platformfree <: SESSION_TYPE
end



