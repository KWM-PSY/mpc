# ===========================================================
#  Copyright (C) 2022 Gerda Wyssen, Daniel Fitze and Matthias Ertl, University of Bern,
#  matthias.ertl@unibe.ch
#  gerda.wyssen@unibe.ch 
#  daniel.fitze@unibe.ch # 
#
#  This file is part of mpc.
# 
#  mpc is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
# 
#  mpc is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with mpc. If not, see <http://www.gnu.org/licenses/>.
#  ===========================================================
@kwdef struct gl_sphere <: primitive 
  name::String = "sph1"
  radius::Float64 = 5.0
  refinements::Int = 5
  ambient::color = mpc.red()
  diffuse::color = mpc.red()
  specular::color = mpc.red()
  shine::Float64 = 1.0
  position::position = mpc.position3d()
  orientation::orientation = mpc.orientation()
end

@kwdef struct gl_cylinder <: primitive 
  name::String = "cyl1"
  radius::Float64 = 5.0
  length::Float64 = 10.0
  refinements::Int = 5
  ambient::color = mpc.red()
  diffuse::color = mpc.red()
  specular::color = mpc.red()
  shine::Float64 = 1.0
  position::position = mpc.position3d()
  orientation::orientation = mpc.orientation()
end

@kwdef struct gl_torus <: primitive 
  name::String = "tor1"
  radius::Float64 = 5.0
  length::Float64 = 10.0
  refinements::Int = 5
  ambient::color = mpc.red()
  diffuse::color = mpc.red()
  specular::color = mpc.red()
  shine::Float64 = 1.0
  position::position = mpc.position3d()
  orientation::orientation = mpc.orientation()
end

@kwdef struct gl_tetrahedron <: primitive 
  name::String = "tet1"
  length::Float64 = 10.0
  ambient::color = mpc.red()
  diffuse::color = mpc.red()
  specular::color = mpc.red()
  shine::Float64 = 1.0
  position::position = mpc.position3d()
  orientation::orientation = mpc.orientation()
end

@kwdef struct gl_parallelepiped <: primitive 
  name::String = "par"
  width::Float64 = 10.0
  length::Float64 = 10.0
  depth::Float64 = 10.0
  ambient::color = mpc.red()
  diffuse::color = mpc.red()
  specular::color = mpc.red()
  shine::Float64 = 1.0
  position::position = mpc.position3d()
  orientation::orientation = mpc.orientation()
end





