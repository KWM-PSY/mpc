@kwdef struct btn
    btn_labels::Vector{String} = ["Links", "Rechts"]
end

@kwdef struct gamepad
    btn_nb::Int = 2
    btn_names::Vector{String} = ["fl1", "fr1"]
end

@kwdef mutable struct likert
    pos::Int = 4
    resp::Union{Int, Nothing} = nothing
    done::Bool = false
end

@kwdef struct input
    btn::Union{btn, Nothing} = nothing
    gamepad::Union{gamepad, Nothing} = nothing
    likert::Union{likert, Nothing} = nothing
    joystick::Union{joystick, Nothing} = nothing
end
