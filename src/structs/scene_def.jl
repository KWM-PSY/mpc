# ===========================================================
#  Copyright (C) 2022 Gerda Wyssen, Daniel Fitze and Matthias Ertl, University of Bern,
#  matthias.ertl@unibe.ch
#  gerda.wyssen@unibe.ch 
#  daniel.fitze@unibe.ch # 
#
#  This file is part of mpc.
# 
#  mpc is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
# 
#  mpc is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with mpc. If not, see <http://www.gnu.org/licenses/>.
#  ===========================================================
@kwdef mutable struct simple_screen <: screen_type 
  screen_type::Array{screen_type} = [mpc.screen()]
  virtual_screen::Array{virtual_screen} = [mpc.flat()]
  light::Array{light_source} = [mpc.light_source()]
end

@kwdef mutable struct simple_vr <: screen_type 
  screen_type::Array{screen_type} = [mpc.vr(), mpc.screen()]
  virtual_screen::Array{virtual_screen} = [mpc.flat()]
  light::Array{light_source} = [mpc.light_source()]
end












