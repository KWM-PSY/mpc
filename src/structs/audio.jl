# ===========================================================
#  Copyright (C) 2022 Gerda Wyssen, Daniel Fitze and Matthias Ertl, University of Bern,
#  matthias.ertl@unibe.ch
#  gerda.wyssen@unibe.ch 
#  daniel.fitze@unibe.ch # 
#
#  This file is part of mpc.
# 
#  mpc is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
# 
#  mpc is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with mpc. If not, see <http://www.gnu.org/licenses/>.
#  ===========================================================
"""
exit_on: sample_start | sample_end
"""
@kwdef struct audio <: EVENT
  name::String = "white_noise"
  chan::Int = 0
  volume::Float64 = 1.0
  repetitions::Int = 0
  exit_on::String = "sample_start"
end

@kwdef struct interrupt_audio <: EVENT 
    audio::audio
    exit_on::String = "immediate"
end

@kwdef struct audio_srv
    audio_dir::String = "old"
    nb_channel::Int = 2
end


