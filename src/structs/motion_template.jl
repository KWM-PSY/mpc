# ===========================================================
#  Copyright (C) 2021 Gerda Wyssen, Daniel Fitze and Matthias Ertl, University of Bern,
#  matthias.ertl@unibe.ch
#  gerda.wyssen@unibe.ch 
#  daniel.fitze@unibe.ch # 
#
#  This file is part of mpc.
# 
#  mpc is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
# 
#  mpc is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with mpc. If not, see <http://www.gnu.org/licenses/>.
#  ===========================================================
"""
exit_on: "motion" | "btn"
"""
@kwdef struct motion_template <: EVENT
    dof::pltf_position = mpc.pltf_position() 
    duration::Union{Vector{Float64}, Float64, Nothing} = nothing 
    law::String = "S"
    max_actuator_speed::Float64 = 200.0
    rotcenter::POSITION = mpc.position3d(0.0, 0.0, 0.0) 
    exit_on::String = "motion"
    reverse::Bool = false
    log::Bool = true
end

@kwdef struct velocity_step <: EVENT
    step_ID::Int
    dof::pltf_position = mpc.pltf_position() 
end


