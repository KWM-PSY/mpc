# ===========================================================
#  Copyright (C) 2021 Gerda Wyssen, Daniel Fitze, Matthias Ertl and Cedric Berther, University of Bern,
#  gerda.wyssen@unibe.ch, daniel.fitze@unibe.ch, matthias.ertl@unibe.ch, cedric.berther@unibe.ch
# 
#  This file is part of mpc.
# 
#  mpc is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
# 
#  mpc is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with mpc.  If not, see <http://www.gnu.org/licenses/>.
#  ===========================================================

module mpc

import Base.Threads.@spawn
using Base:@kwdef 
using InlineTest, Printf, Libdl, Sockets, Distributions, StringEncodings, Dates, CSV, DataFrames, Random


# TYPES
# ---------------------------------------
#include("structs/generic_def.jl")
#include("structs/trigger_def.jl")
#include("structs/hardware_def.jl")
#include("structs/gvs_def.jl")
#include("structs/string_def.jl")
#include("structs/meta_def.jl")
#include("structs/kernel_def.jl")
#include("structs/glw_def.jl")
#include("structs/scene_def.jl")
#include("structs/primitive_def.jl")
#include("structs/sequence_def.jl")
#include("structs/trial_def.jl")

# STATE
# =======================================
# client pc -----------------------------

# pltf ----------------------------------
abstract type LAB_CONFIG end
abstract type SESSION_TYPE end
abstract type POSITION end
  include("structs/pltf_position.jl")
  include("structs/lab_config.jl")
  include("state_thread/load_pltf_const.jl")


# OUTPUT
# =======================================
abstract type OUTPUT end
abstract type EVENT end
abstract type DISPLAY end
# motion --------------------------------
  include("structs/position.jl")
  include("structs/session_types.jl")
  include("structs/motion_template.jl")
  include("motion/start_short_motion.jl")
  include("motion/def_motion_steps.jl")
  include("motion/prepare_motion.jl")
  include("motion/change_velocity.jl")
# audio ---------------------------------
  include("structs/audio.jl")
  include("audio/play_audio.jl")
  include("audio/stop_audio.jl")
  include("audio/set_volume.jl")
# img -----------------------------------
  include("structs/img.jl")
  include("img/ipd.jl")
  include("img/prepare_disp_msg.jl")
  include("img/disp_img.jl")
  include("img/get_img_names.jl")
# transducer ----------------------------
  include("structs/transducer.jl")

  include("structs/output.jl")
# logfile (bids) ------------------------
  include("generic_functions/bids.jl")


# INPUT
# =======================================
abstract type INPUT end
# likert --------------------------------
  include("structs/input.jl")

# STATE THREAD
# ---------------------------------------
import Base.Threads.@spawn
include("structs/config_type.jl")
include("structs/state.jl")
include("state_thread/loop.jl")
include("state_thread/run_loop.jl")
include("state_thread/update_status.jl")
include("state_thread/wait_for.jl")
include("state_thread/print_timeout.jl")

# SERVER-CLIENT INTERFACE
# ---------------------------------------
include("interface/Port.jl")
include("interface/constants.jl")
include("interface/sendmsg.jl")
include("interface/get_packet.jl")
include("interface/close.jl")
include("interface/log2server.jl")
#include("messages/messages.jl")


# GENERIC FUNCTIONS
# ---------------------------------------
include("generic_functions/login.jl")
include("generic_functions/engage.jl")
include("generic_functions/start_session.jl")
#include("generic_functions/start_platformfree_session.jl")
include("generic_functions/park.jl")
include("generic_functions/logout.jl")
include("generic_functions/end_session.jl")
include("generic_functions/check_topic.jl")
include("generic_functions/check_position.jl")
include("generic_functions/start_in_out.jl")
include("generic_functions/get_log.jl")
#include("generic_functions/set_random_seed.jl")
#include("generic_functions/do_post_production.jl")
#include("generic_functions/list_sessions.jl")



# SUBJECT INTERACTION
# ---------------------------------------
#include("generic_functions/activate_hw_input.jl")
#include("generic_functions/get_button_press.jl")
include("generic_functions/get_input.jl")
#include("generic_functions/log_data.jl")



# GL-World
# ---------------------------------------
#include("glworld/open_glworld.jl")
#include("glworld/add_glworld_screen.jl")
#include("glworld/add_glworld_light.jl")
#include("glworld/add_glworld_primitive.jl")
#include("glworld/close_sequence.jl")
#include("glworld/activate_glworld.jl")
#include("glworld/camera_glworld.jl")
#include("glworld/fov_glworld.jl")
#include("glworld/set_visible_glworld.jl")
#include("glworld/set_glworld_position.jl")
#include("glworld/set_screen_color.jl")
#include("glworld/set_screen_image.jl")
#include("glworld/set_screen_string.jl")
#include("glworld/set_ipd.jl")
#include("glworld/get_ipd.jl")
#include("glworld/adjust_ipd.jl")
#include("glworld/init_glworld.jl")
#include("glworld/scenes/simple_screen.jl")
#include("glworld/scenes/simple_vr.jl")
#include("glworld/add_sequence_step.jl")


# GVS
# ---------------------------------------
#include("gvs/open_gvs.jl")


# YAW
# ---------------------------------------
include("generic_functions/yaw_set_ref_pos.jl")

# TRIAL
# ---------------------------------------
include("structs/btn_listener.jl")
include("structs/trial_log.jl")
include("structs/wait.jl")
include("structs/likert_resp.jl")
include("structs/trial_structure.jl")
include("trial/setup_likert.jl")
include("trial/update_likert.jl")
include("trial/execute_event.jl")
include("trial/run_trial.jl")
include("trial/check_trial_state.jl")
include("trial/log_trial_state.jl")

# SPECIAL TOPICS
# ---------------------------------------
#include("special/feedback/feedback_add_horizon.jl")
include("structs/perturbation_def.jl")
include("special/feedback/feedback_add_perturbation.jl")
include("special/feedback/feedback_pause.jl")
include("special/feedback/feedback_start.jl")

# ABSTRACT FUNCTIONS
# ---------------------------------------
include("abstract_functions/exit_on.jl")
include("abstract_functions/moog_param_test.jl")


# HELPER
# ---------------------------------------
include("helper/print_msg.jl")
include("helper/get_input.jl")
include("parport/parport.jl")
#include("helper/helper.jl")
#include("helper/kernel.jl")

end # module
