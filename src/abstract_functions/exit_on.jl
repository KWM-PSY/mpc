# ===========================================================
#  Copyright (C) 2022 Gerda Wyssen, Daniel Fitze, Matthias Ertl, University of Bern,
#  gerda.wyssen@unibe.ch, daniel.fitze@unibe.ch, matthias.ertl@unibe.ch
# 
#  This file is part of mpc.
# 
#  mpc is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
# 
#  mpc is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with mpc.  If not, see <http://www.gnu.org/licenses/>.
#  ===========================================================

"""
possible event strings are:
"motion" => waits on sts.motion_state (SHORT_SEQ_ACK)
"motion+btn" => first waits on motion and second waits on btn
"""
function exit_on(event::motion_template)

    if !(event.exit_on in ["motion", "btn", "immediate"]) 
        mpc.print_msg("Warning", "Invalid event string!") 
    end

    if event.exit_on == "immediate"
        # dont wait for anything to start events on time
        return
    end

    mpc.wait_for(type = mpc.sts.output_state,
                 parameter = "motion_state",
                 msg_ack = mpc.MSG_SHORT_SEQ_ACK,
                 msg_nak = mpc.MSG_SHORT_SEQ_NAK,
                 out_ack = "Short sequence acknowledged!",
                 out_nak = "Something went wrong!")
    mpc.wait_for(type = mpc.sts.output_state,
                 parameter = "motion_state",
                 msg_ack = mpc.MSG_SHORT_SEQ_DONE,
                 msg_nak = mpc.MSG_SHORT_SEQ_NAK,
                 out_ack = "Short sequence done!",
                 out_nak = "Something went wrong!")
    if event.exit_on == "btn"
        if mpc.sts.input_state.listen_to_btn == false 
            mpc.print_msg("Warning", "Not listening to btns!") 
        end
        mpc.wait_for(type = mpc.sts.input_state,
                     parameter = "btn_pressed",
                     msg_ack = true, 
                     msg_nak = mpc.MSG_SHORT_SEQ_NAK, 
                     out_ack = "Btn was pressed", 
                     out_nak = "Something went wrong!",
                     timeout = 60,
                     print_timeout = true)
    end
end

function exit_on(event::audio)
    if !(event.exit_on in ["sample_start", "sample_end", "immediate"]) 
        mpc.print_msg("Warning", "Invalid event string!") 
        println("*******", event.name)
    end

    if event.exit_on == "immediate"
        # dont wait for anything to start events on time
        return
    end

    mpc.wait_for(type = mpc.sts.output_state,
                 parameter = "audio_state",
                 msg_ack = mpc.MSG_AV_AUDIO_ACK,
                 msg_nak = mpc.MSG_AV_AUDIO_NAK,
                 out_ack = "audio sample playing",
                 out_nak = "something went wrong")
    if event.exit_on == "sample_end"
        mpc.wait_for(type = mpc.sts.output_state,
                     parameter = "audio_state",
                     msg_ack = mpc.MSG_AV_AUDIO_SAMPLE_ENDED,
                     msg_nak = mpc.MSG_AV_AUDIO_NAK,
                     out_ack = "audio sample ended",
                     out_nak = "something went wrong",
                     timeout = 60)
    end
end

function exit_on(event::interrupt_audio)
    if event.exit_on == "immediate"
        # dont wait for anything to start events on time
        return
    end
end
    

function exit_on(event::img_stim)
    if !(event.exit_on in ["ack", "immediate"]) 
        mpc.print_msg("Warning", "Invalid event string!") 
    end

    if event.exit_on == "immediate"
        # dont wait for anything to start events on time
        return
    end

    mpc.wait_for(type = mpc.sts.output_state,
                 parameter = "img_state",
                 msg_ack = event.name,
                 msg_nak = mpc.MSG_AV_GLW_NAK,
                 out_ack = "img $(event.name) displayed",
                 out_nak = "something went wrong")
end

function exit_on(event::wait)
    if !(event.on in ["btn", "motion", "motion+btn"]) 
        mpc.print_msg("Warning", "Invalid event string!") 
    end

    if event.on == "btn"
        mpc.wait_for(type = mpc.sts.input_state,
                     parameter = "btn_pressed",
                     msg_ack = true, 
                     msg_nak = mpc.MSG_SHORT_SEQ_NAK, 
                     out_ack = "Btn was pressed", 
                     out_nak = "Something went wrong!",
                     timeout = 600,
                     print_timeout = true)
    elseif event.on == "motion"
        mpc.wait_for(type = mpc.sts.output_state,
                     parameter = "motion_state",
                     msg_ack = mpc.MSG_SHORT_SEQ_ACK,
                     msg_nak = mpc.MSG_SHORT_SEQ_NAK,
                     out_ack = "Short sequence acknowledged!",
                     out_nak = "Something went wrong!")
        mpc.wait_for(type = mpc.sts.output_state,
                     parameter = "motion_state",
                     msg_ack = mpc.MSG_SHORT_SEQ_DONE,
                     msg_nak = mpc.MSG_SHORT_SEQ_NAK,
                     out_ack = "Short sequence done!",
                     out_nak = "Something went wrong!")
    elseif event.on == "motion+btn"
        mpc.wait_for(type = mpc.sts.output_state,
                     parameter = "motion_state",
                     msg_ack = mpc.MSG_SHORT_SEQ_ACK,
                     msg_nak = mpc.MSG_SHORT_SEQ_NAK,
                     out_ack = "Short sequence acknowledged!",
                     out_nak = "Something went wrong!")
        mpc.wait_for(type = mpc.sts.output_state,
                     parameter = "motion_state",
                     msg_ack = mpc.MSG_SHORT_SEQ_DONE,
                     msg_nak = mpc.MSG_SHORT_SEQ_NAK,
                     out_ack = "Short sequence done!",
                     out_nak = "Something went wrong!")
        mpc.wait_for(type = mpc.sts.input_state,
                     parameter = "btn_pressed",
                     msg_ack = true, 
                     msg_nak = mpc.MSG_SHORT_SEQ_NAK, 
                     out_ack = "Btn was pressed", 
                     out_nak = "Something went wrong!",
                     timeout = 600,
                     print_timeout = true)

    end
end
