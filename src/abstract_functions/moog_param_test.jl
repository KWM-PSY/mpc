# ===========================================================
#  Copyright (C) 2022 Gerda Wyssen, Daniel Fitze, Matthias Ertl, University of Bern,
#  gerda.wyssen@unibe.ch, daniel.fitze@unibe.ch, matthias.ertl@unibe.ch
# 
#  This file is part of mpc.
# 
#  mpc is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
# 
#  mpc is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with mpc.  If not, see <http://www.gnu.org/licenses/>.
#  ===========================================================

function run_moog_param_test(;sampling_frequency = 200)
    global cfg = mpc.config(platform_model = "Moog_2",
                            sampling_freq = sampling_frequency,
                            client_ip = "10.33.1.50", 
                            server_ip = "10.33.1.1",
                            server_outport = 1860,
                            server_inport = 1966,
                            disp_info = true,
                            path_data = "../data/moog_param_test/",
                            log_resolution = 1,
                            mailto = "",
                            session = mpc.short(btn_labels = [],
                                                topic = "master",
                                                actuator_mode = "D",
                                                start_height = 670))
    mpc.run_loop(cfg)
    mpc.login(cfg.session,
              do_engage = true)
end


