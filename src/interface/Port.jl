# ===========================================================
#  Copyright (C) 2022 Gerda Wyssen, Daniel Fitze, Matthias Ertl, University of Bern,
#  gerda.wyssen@unibe.ch, daniel.fitze@unibe.ch, matthias.ertl@unibe.ch
# 
#  This file is part of mpc.
# 
#  mpc is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
# 
#  mpc is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with mpc.  If not, see <http://www.gnu.org/licenses/>.
#  ===========================================================

mutable struct Port
  remport::Int
  sts1::Int
  sts2::Int
  sts3::Int
  wait_for_idle::Bool
  dls_sendpkt
  dls_recvpkt
  dls_close
  
  function Port(;platform_model = MOTION_PLATFORM, client_ip = LOCAL_IP, server_ip = SERVER_IP, server_outport = SERVER_OUTPORT, server_inport = SERVER_INPORT)

	  # check validity of the parameter input of "platform_model". see "const platforms for valid devices.
	  if sum(occursin.(platforms, platform_model)) < 1
		  # in message dings eintragen
		  error("Platform model $platform_model is not a valid device")
	  end
    
    dlo=dlopen(MOOGSKT_C_MODULE2)
    dls_init=dlsym(dlo,:moogskt_init)
    dls_recvport=dlsym(dlo,:moogskt_recvport)
    dls_sendpkt=dlsym(dlo,:moogskt_send_pkt)
    dls_recvpkt=dlsym(dlo,:moogskt_recv_pkt)
    dls_close=dlsym(dlo,:moogskt_close)

    err=ccall(dls_init,Cint,(Cstring,Cstring,Cstring,Cint,Cint), 
	      platform_model,
	      client_ip, 
	      server_ip,
              server_outport, 
	      server_inport)
    if(err!=0)
      error("Could not contact platform")
    end
    port=ccall(dls_recvport,Cint,())
    
    new(port,-1,-1,-1,false,dls_sendpkt,dls_recvpkt,dls_close)
  end
end

#@testset "interface.Port" begin
#  p = mpc.Port()
#  @test p.remport > 0 && p.remport < 50000
#  @test typeof(a.dls_close) == Ptr{Nothing}
#  @test typeof(a.dls_recvpkt) == Ptr{Nothing}
#  @test typeof(a.dls_sendpkt) == Ptr{Nothing}
#  @test a.sts1 == -1
#  @test a.sts2 == -1
#  @test a.sts3 == -1
#  @test a.wait_for_idle == false
#  mpc.close(p)
#
#  b = mpc.interface.Port("Moog_1")
#  @test b.remport > 0 && b.remport < 50000
#  @test typeof(b.dls_close) == Ptr{Nothing}
#  @test typeof(b.dls_recvpkt) == Ptr{Nothing}
#  @test typeof(b.dls_sendpkt) == Ptr{Nothing}
#  @test b.sts1 == -1
#  @test b.sts2 == -1
#  @test b.sts3 == -1
#  @test b.wait_for_idle == false
#  mpc.interface.close(b)
#end
#
