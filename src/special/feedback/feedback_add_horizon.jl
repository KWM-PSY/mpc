# ===========================================================
#  Copyright (C) 2022 Gerda Wyssen, Daniel Fitze and Matthias Ertl, University of Bern,
#  matthias.ertl@unibe.ch
#  gerda.wyssen@unibe.ch 
#  daniel.fitze@unibe.ch 
# 
#  This file is part of mpc.
# 
#  mpc is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
# 
#  mpc is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with mpc. If not, see <http://www.gnu.org/licenses/>.
#  ===========================================================
function feedback_add_horizon(type = "DJ", display::vr)
    mpc.sendmsg(mpc.MSG_FEEDBACK_ADD_HORIZON, 
                @sprintf("%s,%s,%s", 
                         type,
                         "V",
                         display.type))
end

function feedback_add_horizon(type = "DJ", display::screen)
    mpc.sendmsg(mpc.MSG_LOGIN, 
                @sprintf("%s,%s,%d,%d", 
                         type,
                         "W",
                         display.resolution[1],
                         display.resolution[2]))
end



