# ===========================================================
#  Copyright (C) 2022 Gerda Wyssen, Daniel Fitze and Matthias Ertl, University of Bern,
#  matthias.ertl@unibe.ch
#  gerda.wyssen@unibe.ch 
#  daniel.fitze@unibe.ch 
# 
#  This file is part of mpc.
# 
#  mpc is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
# 
#  mpc is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with mpc. If not, see <http://www.gnu.org/licenses/>.
#  ===========================================================
function feedback_pause()
    println("Pause feedback")
    mpc.sendmsg(mpc.MSG_FEEDBACK_PAUSE, 
                @sprintf("1")) 

    mpc.wait_for(type = mpc.sts,
                 parameter = "park_state",
                 msg_ack = mpc.MSG_FEEDBACK_ACK,
                 msg_nak = mpc.MSG_FEEDBACK_NAK,
                 out_ack = "pause acknowledged",
                 out_nak = "pause failed",
                 timeout = 30.0)
end

function feedback_resume()
    mpc.sendmsg(mpc.MSG_FEEDBACK_PAUSE, 
                @sprintf("0")) 
end

function feedback_park()
    mpc.sendmsg(mpc.MSG_FEEDBACK_PARK,
                @sprintf(""))

    mpc.wait_for(type = mpc.sts,
                 parameter = "park_state",
                 msg_ack = mpc.MSG_FEEDBACK_ACK,
                 msg_nak = mpc.MSG_FEEDBACK_NAK,
                 out_ack = "pause acknowledged",
                 out_nak = "pause failed",
                 timeout = 30.0)

end

