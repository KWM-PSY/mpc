function setup_likert(;screen)
    resp = mpc.likert_resp()
    L = []
    for i in 1:length(resp.imgs_names)
        tmp = mpc.img_stim(screen = screen,
                           name = resp.imgs_names[i],
                           exit_on = "ack")
        push!(L, tmp)
    end
    resp.imgs = L
    return resp
end

