function update_likert(L::likert_resp)
    mpc.sts.input_state.listen_to_btn = true
    mpc.execute_event(L.imgs[mpc.sts.input_state.likert_state.pos])
    while mpc.sts.input_state.likert_state.done == false
        sleep(0.1)
        #@show mpc.sts.input_state.likert_state.pos
        #@show mpc.sts.input_state.btn
        if  mpc.sts.input_state.btn != nothing
            if mpc.sts.input_state.likert_state.pos < 7 && 
                mpc.sts.input_state.btn[end] == "fr1"
                mpc.sts.input_state.likert_state.pos += 1
                mpc.execute_event(L.imgs[mpc.sts.input_state.likert_state.pos])
                mpc.sts.input_state.btn = nothing
            elseif mpc.sts.input_state.likert_state.pos > 1 &&  
                mpc.sts.input_state.btn[end]  == "fl1"
                mpc.sts.input_state.likert_state.pos -= 1
                mpc.execute_event(L.imgs[mpc.sts.input_state.likert_state.pos])
                mpc.sts.input_state.btn = nothing
            elseif mpc.sts.input_state.btn[end] == "a"
                mpc.sts.input_state.likert_state.done = true
                mpc.execute_event(L.empty_screen)
            end
        end
    end
    mpc.sts.input_state.listen_to_btn = false
    tmp = mpc.log_trial(mpc.sts.input_state.likert_state)
    return tmp
end


