# ===========================================================
#  Copyright (C) 2022 Gerda Wyssen, Daniel Fitze and Matthias Ertl, University of Bern,
#  matthias.ertl@unibe.ch
#  gerda.wyssen@unibe.ch 
#  daniel.fitze@unibe.ch 
# 
#  This file is part of mpc.
# 
#  mpc is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
# 
#  mpc is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with mpc. If not, see <http://www.gnu.org/licenses/>.
#  ===========================================================
function run_trial(trial::trial_structure; log::Bool = true)

    if log
        global tmp_log = mpc.trial_log()
    end

    @show tmp_log
    readline()
    # check sts.triale_state
    mpc.check_trial_state(reset = true)


    start_time = time()
    next_event = 1

    while next_event <= length(trial.event_time)
        # check if event needs to be executed
        if time()-start_time >= trial.event_time[next_event][2]

            # execute event
            mpc.execute_event(trial.event_time[next_event][1])

            # log trial
            if log
                mpc.log_event(trial.event_time[next_event][1])
            end
            #if hasproperty(trial.event_time[next_event][1], :log)
            #    push!(trial_log, mpc.log_trial(trial.event_time[next_event]))
            #end

            @show tmp_log
            readline()
            next_event += 1
        end
    end
    return trial_log
end




