# ===========================================================
#  Copyright (C) 2022 Gerda Wyssen, Daniel Fitze, Matthias Ertl, University of Bern,
#  gerda.wyssen@unibe.ch, daniel.fitze@unibe.ch, matthias.ertl@unibe.ch
# 
#  This file is part of mpc.
# 
#  mpc is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
# 
#  mpc is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with mpc.  If not, see <http://www.gnu.org/licenses/>.
#  ===========================================================
function log_event(event::img_stim)
    if !@isdefined(tmp_log)
        mpc.print_msg("Warning", "tmp log not defined")
        return
    end

    tmp_log.img_log = mpc.img_log(name = event.name)
end

function log_event(event::likert)
    if !@isdefined(tmp_log)
        mpc.print_msg("Warning", "tmp log not defined")
        return
    end
    tmp_log.input_log.likert_pos = mpc.sts.input_state.likert_state
    # clear sts.trial_state
    #mpc.sts.output_state.motion_start = nothing
    #mpc.sts.output_state.motion_end = nothing
    #mpc.sts.input_state.btn_pressed = nothing
    #mpc.sts.input_state.btn = nothing
    #mpc.sts.input_state.btn_time = nothing
    #mpc.sts.input_state.likert_state = mpc.likert()
end

#function log_trial(event::Tuple{EVENT,Float64})
#
#    tmp = nothing
#
#    # get and log info
#    if event[1].log
#
#        tmp = mpc.trial_log(event = (string(typeof(event[1])), event[2]),
#                            motion_start = mpc.sts.output_state.motion_start,
#                            motion_end = mpc.sts.output_state.motion_end,
#                            btn_pressed = mpc.sts.input_state.btn_pressed,
#                            btn = mpc.sts.input_state.btn,
#                            btn_time = mpc.sts.input_state.btn_time,
#                            likert_state = mpc.sts.input_state.likert_state)
#
#    end
#
#    # clear sts.trial_state
#    mpc.sts.output_state.motion_start = nothing
#    mpc.sts.output_state.motion_end = nothing
#    mpc.sts.input_state.btn_pressed = nothing
#    mpc.sts.input_state.btn = nothing
#    mpc.sts.input_state.btn_time = nothing
#    mpc.sts.input_state.likert_state = mpc.likert()
#
#    return tmp
#end


