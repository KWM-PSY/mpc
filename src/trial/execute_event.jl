# ==========================================================
#  Copyright (C) 2022 Gerda Wyssen, Daniel Fitze, Matthias Ertl, University of Bern,
#  gerda.wyssen@unibe.ch, daniel.fitze@unibe.ch, matthias.ertl@unibe.ch
# 
#  This file is part of mpc.
# 
#  mpc is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
# 
#  mpc is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with mpc.  If not, see <http://www.gnu.org/licenses/>.
#  ===========================================================

function execute_event(event::motion_template)
    mpc.start_short_motion(event)
    mpc.exit_on(event)
end

function execute_event(event::audio)
    mpc.play_audio(event)
    mpc.exit_on(event)
end

function execute_event(event::interrupt_audio)
    mpc.stop_audio(event.audio)
    mpc.exit_on(event)
end

function execute_event(event::img_stim)
    mpc.disp_img(event)
    mpc.exit_on(event)
end

function execute_event(event::likert_resp)
    mpc.update_likert(event)
end

function execute_event(event::wait)
    mpc.exit_on(event)
end

function execute_event(event::btn_switch)
    mpc.sts.input_state.listen_to_btn = event.listening
end

#function execute_event(event::trigger)
#  mpc.send_trigger(event)
#end



