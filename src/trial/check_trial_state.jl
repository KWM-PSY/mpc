# ===========================================================
#  Copyright (C) 2022 Gerda Wyssen, Daniel Fitze, Matthias Ertl, University of Bern,
#  gerda.wyssen@unibe.ch, daniel.fitze@unibe.ch, matthias.ertl@unibe.ch
# 
#  This file is part of mpc.
# 
#  mpc is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
# 
#  mpc is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with mpc.  If not, see <http://www.gnu.org/licenses/>.
#  ===========================================================

function check_trial_state(;reset = true)
    empty = true
    inp_fields = ["btn_pressed", "btn", "btn_time", "likert_state"]
    for field in inp_fields
        value = getfield(mpc.sts.input_state, Symbol(field))
        isnothing(value) ? empty = empty : empty = false
    end
    
    out_fields = ["motion_start", "motion_end"]
    for field in out_fields
        value = getfield(mpc.sts.output_state, Symbol(field))
        isnothing(value) ? empty = empty : empty = false
    end

    if reset && !empty
        mpc.print_msg("Warning", "trial state is cleared!")
        for field in inp_fields
            if field == "likert_state"
                setfield!(mpc.sts.input_state, Symbol(field), mpc.likert())
            else
                setfield!(mpc.sts.input_state, Symbol(field), nothing)
            end
        end
        for field in out_fields
            setfield!(mpc.sts.output_state, Symbol(field), nothing)
        end
        empty = true
    end
    return empty
end
