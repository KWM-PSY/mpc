module messages

const msg_1 = "user_spec is not supported yet."
const msg_2 = "LOGIN successfull!"
const msg_3 = "LOGIN failed! - Platform did not ack login: "
const msg_4 = " mode is not yet implemented!"
const msg_5 = "TOPIC accepted!"
const msg_6 = "Wrong TOPIC selected! "
const msg_7 = "ENGAGING acknowledged!"
const msg_8 = "ENGAGING successfull!"
const msg_9 = "ENGAGING failed! - Platform did not ack engage: "
const msg_10 = "LOGOUT successfull!"
const msg_11 = "LOGOUT failed! - Platform did not ack logout: "
const msg_12 = "PARKING successfull!"
const msg_13 = "PARKING failed! Platform did not ack park: "
const msg_14 = "DEVICE INPUT successfull!"
const msg_15 = "DEVICE INPUT failed! Platform did not ack activation: "

end
