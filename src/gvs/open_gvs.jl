# ===========================================================
#  Copyright (C) 2022 Gerda Wyssen, Daniel Fitze and Matthias Ertl, University of Bern,
#  matthias.ertl@unibe.ch
#  gerda.wyssen@unibe.ch 
#  daniel.fitze@unibe.ch 
# 
#  This file is part of mpc.
# 
#  mpc is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
# 
#  mpc is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with mpc. If not, see <http://www.gnu.org/licenses/>.
#  ===========================================================
function open_gvs(cfg_session::session_def, cfg_stim::neurocon)

  mg = []
  
  for d = 1:length(cfg_stim.ports)
    mg = join([string(cfg_stim.ports[d]) 
               string(cfg_stim.lower[d])
               string(cfg_stim.upper[d])], 
              "/")
    
    if d < length(cfg_stim.ports)
      mg = mg * "|"
    end
  end
  mg = mg * "," * string(cfg_stim.srate)

  mpc.sendmsg(cfg_session, 
              mpc.MSG_AV_OPEN_TRANSD, 
              mg)
  mpc_session, recv = mpc.srv_resp(cfg_session, 
                                   mpc.MSG_AV_AUDIO_ACK, 
                                   mpc.MSG_AV_AUDIO_NAK,
                                   "[  Opened stimulator  ]",
                                   "[  Something went wrong  ]")

  return cfg_session
end
