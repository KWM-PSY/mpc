# ===========================================================
#  Copyright (C) 2022 Gerda Wyssen, Daniel Fitze and Matthias Ertl, University of Bern,
#  matthias.ertl@unibe.ch
#  gerda.wyssen@unibe.ch 
#  daniel.fitze@unibe.ch 
# 
#  This file is part of mpc.
# 
#  mpc is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
# 
#  mpc is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with mpc. If not, see <http://www.gnu.org/licenses/>.
#  ===========================================================
function play_gvs(cfg_session::cfg_session, cfg_gvs::gvs_profile)

  mg = @sprintf("%d,%s,%d,%d,%d,%d", 
                cfg_gvs.chan, 
                cfg_gvs.name, 
                cfg_gvs.lower, 
                cfg_gvs.upper, 
                cfg_gvs.interrupt, 
                cfg_gvs.repetitions)

  mpc.sendmsg(cfg_session, 
              mpc.MSG_AV_TRANSD_PLAY, 
              mg)
  mpc_session, recv = mpc.srv_resp(cfg_session, 
                                   mpc.MSG_AV_TRANSD_ACK, 
                                   mpc.MSG_AV_TRANSD_NAK,
                                   "[  Playing stimulation $cfg_gvs.name on channel $cfg_gvs.chan  ]",
                                   "[  Something went wrong  ]")

  return cfg_session
end
