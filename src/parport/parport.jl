# ===========================================================
#  Copyright (C) 2022 Gerda Wyssen, Daniel Fitze and Matthias Ertl, University of Bern,
#  matthias.ertl@unibe.ch
#  gerda.wyssen@unibe.ch 
#  daniel.fitze@unibe.ch 
# 
#  This file is part of mpc.
# 
#  mpc is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
# 
#  mpc is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with mpc. If not, see <http://www.gnu.org/licenses/>.
#  ===========================================================
function init_parport(;port_nb::Int = 0, pulse_length::Int = 10000)
  
  mg = @sprintf("%s,%s",string(port_nb), string(pulse_length))
  mpc.sendmsg(mpc.MSG_ASSOCIATE_PARPORT, 
              mg)
end

function send_trigger(trigger_number)
  mpc.sendmsg(mpc.MSG_PARPORT_SEND, 
              string(trigger_number))
end

#function define_trigger(cfg_event::trigger_image)
#  mpc.sendmsg(mpc.MSG_PARPORT_LATCH, 
#              @sprintf("%s,%s,%s", string(0), 
#                                   cfg_event.screen, 
#                                   string(cfg_event.onset)))
#end

#function define_trigger(cfg_event::trigger_movie)
#  mpc.sendmsg(mpc.MSG_PARPORT_LATCH, 
#              @sprintf("%s,%s,%s", string(1), 
#                                   cfg_event.screen, 
#                                   string(cfg_event.onset)))
#  
#  mpc.sendmsg(mpc.MSG_PARPORT_LATCH, 
#              @sprintf("%s,%s,%s", string(2), 
#                                   cfg_event.screen, 
#                                   string(cfg_event.offset)))
#end

#function define_trigger(cfg_event::trigger_audio)
#  mpc.sendmsg(mpc.MSG_PARPORT_LATCH, 
#              @sprintf("%s,%s", string(3), 
#                                string(cfg_event.onset)))
#
#  mpc.sendmsg(mpc.MSG_PARPORT_LATCH, 
#              @sprintf("%s,%s", string(4), 
#                                string(cfg_event.offset)))
#end

#function define_trigger(cfg_event::trigger_gvs)
#  mpc.sendmsg(mpc.MSG_PARPORT_LATCH, 
#              @sprintf("%s,%s,%s", string(5), 
#                                   cfg_event.screen, 
#                                   string(cfg_event.onset)))
#  mpc.sendmsg(mpc.MSG_PARPORT_LATCH, 
#              @sprintf("%s,%s,%s", string(6), 
#                                   cfg_event.chan, 
#                                   string(cfg_event.offset)))
#end

#function define_trigger(cfg_event::trigger_visible)
#  mpc.sendmsg(mpc.MSG_PARPORT_LATCH, 
#              @sprintf("%s,%s,%s", string(8), 
#                                   cfg_event.screen, 
#                                   string(cfg_event.onset)))
#end


