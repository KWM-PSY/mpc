# ===========================================================
#  Copyright (C) 2022 Gerda Wyssen, Daniel Fitze and Matthias Ertl, University of Bern,
#  matthias.ertl@unibe.ch
#  gerda.wyssen@unibe.ch 
#  daniel.fitze@unibe.ch 
# 
#  This file is part of mpc.
# 
#  mpc is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
# 
#  mpc is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with mpc. If not, see <http://www.gnu.org/licenses/>.
#  ===========================================================
function change_velocity(motion::velocity_step)
    mpc.sendmsg(mpc.MSG_IMMEDIATE_VEL_REQ,
                @sprintf("%d,%f,%f,%f,%f,%f,%f",
                         motion.step_ID,
                         motion.dof.lateral,
                         motion.dof.heave,
                         motion.dof.surge,
                         deg2rad(motion.dof.yaw),
                         deg2rad(motion.dof.pitch),
                         deg2rad(motion.dof.roll)))
end


