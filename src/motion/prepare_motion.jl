# ===========================================================
#  Copyright (C) 2022 Gerda Wyssen, Daniel Fitze, Matthias Ertl, University of Bern,
#  gerda.wyssen@unibe.ch, daniel.fitze@unibe.ch, matthias.ertl@unibe.ch
# 
#  This file is part of mpc.
# 
#  mpc is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
# 
#  mpc is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with mpc.  If not, see <http://www.gnu.org/licenses/>.
#  ===========================================================

"""
reverse --> back to start_dof
  --> does not work, when current_dof is used
"""
function prepare_motion(
        axis_param::axis_param; 
        start_dof::pltf_position = pltf_position(lateral = 0.0, heave = cfg.output.motion.start_height, surge = 0.0, yaw = 0.0, pitch = 0.0, roll = 0.0), 
        exit_on::String = "motion", 
        reverse::Bool = false, 
        log::Bool = false, 
    )

    steps = mpc.steps()
    setfield!(steps, 
              Symbol(axis_param.axis),
              mpc.prepare_steps(axis_param.param))

    motion = mpc.seq_2_motion(start_dof,
                              steps = steps,
                              exit_on = exit_on,
                              reverse = reverse,
                              log = log,
                              rot_center = axis_param.rot_center)

    return motion
end

"""
reverse --> back to start_dof
  --> does not work, when current_dof is used
"""
function prepare_motion(
        axis_param::Vector{axis_param}; 
        start_dof::pltf_position = pltf_position(lateral = 0.0, 
                                                 heave = cfg.output.motion.start_height, 
                                                 surge = 0.0, 
                                                 yaw = 0.0, 
                                                 pitch = 0.0, 
                                                 roll = 0.0), 
        exit_on::String = "motion", 
        reverse::Bool = false, 
        log::Bool = false, 
    )
    
    steps = mpc.steps()

    for ax_pa in axis_param
        setfield!(steps, 
                  Symbol(ax_pa.axis),
                  mpc.prepare_steps(ax_pa.param))
    end

    motion = mpc.seq_2_motion(start_dof,
                              steps = steps,
                              exit_on = exit_on,
                              reverse = reverse,
                              log = log,
                              rot_center = axis_param[1].rot_center)

    return motion
end
