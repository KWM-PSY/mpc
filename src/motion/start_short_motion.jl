# ===========================================================
#  Copyright (C) 2022 Gerda Wyssen, Daniel Fitze and Matthias Ertl, University of Bern,
#  matthias.ertl@unibe.ch
#  gerda.wyssen@unibe.ch 
#  daniel.fitze@unibe.ch 
# 
#  This file is part of mpc.
# 
#  mpc is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
# 
#  mpc is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with mpc. If not, see <http://www.gnu.org/licenses/>.
#  ===========================================================
"""
    start_short_motion(motion::motion_template)

Sends motion_template to platform.

This function should not be called on its own. `execute_event()` handles order sensitive stuff.
"""
function start_short_motion(motion::motion_template)
    # check if moog is engaged
    if mpc.cfg.rc.pltf_model == "Moog_1"
        if mpc.sts.pltf_state != mpc.PLTF_STATE_ENGAGED
            mpc.print_msg("Warning", "must be ENGAGED-state to start motion")
        end
    elseif mpc.cfg.rc.pltf_model == "Moog_2"
        if mpc.sts.pltf_state != mpc.PLTF2_STATE_ENGAGED
            mpc.print_msg("Warning", "must be ENGAGED-state to start motion")
        end
    elseif mpc.cfg.rc.pltf_model == "Yaw_2"
        if mpc.sts.pltf_state != mpc.PLTFYAW_STATE_STARTED
            mpc.print_msg("Warning", "must be STARTED-state to start motion")
        end
    else
        mpc.print_msg("Warning", "unsuported pltf model")
    end

    # check pltf position if steps are provided
    #if length(motion.dof.heave) > 1
    #    pltf_at_position = mpc.check_position(motion.dof)
    #end
   
    # announce motion profile
    mpc.sendmsg(mpc.MSG_BEGIN_SHORT_SEQ,
                @sprintf("%f,%f,%f,%f",
                         motion.max_actuator_speed,
                         motion.rotcenter.x,
                         motion.rotcenter.y,
                         motion.rotcenter.z))
    length_steps = length(getfield(motion.dof, fieldnames(typeof(motion.dof))[1]))
    if motion.reverse == false
        for step = 1:length_steps
            # send motion profile
            mpc.sendmsg(mpc.MSG_SHORT_SEQ_STEP,
                        @sprintf("%f,%f,%f,%f,%f,%f,%f,%s",
                                 motion.dof.lateral[step],
                                 motion.dof.heave[step],
                                 motion.dof.surge[step],
                                 deg2rad(motion.dof.yaw[step]),
                                 deg2rad(motion.dof.pitch[step]),
                                 deg2rad(motion.dof.roll[step]),
                                 motion.duration[step],
                                 motion.law))
            sleep(0.00001)
        end
    else
        for step = length_steps:-1:1
            # send motion profile in reverse
            mpc.sendmsg(mpc.MSG_SHORT_SEQ_STEP,
                        @sprintf("%f,%f,%f,%f,%f,%f,%f,%s",
                                 motion.dof.lateral[step],
                                 motion.dof.heave[step],
                                 motion.dof.surge[step],
                                 deg2rad(motion.dof.yaw[step]),
                                 deg2rad(motion.dof.pitch[step]),
                                 deg2rad(motion.dof.roll[step]),
                                 motion.duration[step],
                                 motion.law))
            sleep(0.00001)
        end
    end

    # close sequence transmission
    mpc.sendmsg(mpc.MSG_COMPLETE_SHORT_SEQ, "")
end


