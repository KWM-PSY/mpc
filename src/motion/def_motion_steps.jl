# ===========================================================
# motion (part of the mpc module)
# ===========================================================
# Copyright (C) 2022 Matthias Ertl, University of Bern,
#                                   matthias.ertl@unibe.ch
#                    Daniel Fitze, University of Bern,
#                                  daniel.fitze@unibe.ch
#
# This file is part of Platform Commander.
#
# Platform Commander is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Platform Commander is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Platform Commander. If not, see <http://www.gnu.org/licenses/>.
# ===========================================================
abstract type STEP_PARAM end

@kwdef mutable struct axis_param
    axis::String
    param::STEP_PARAM
    rot_center::position3d = position3d(0.0,0.0,0.0)
end

@kwdef mutable struct steps
    lateral::Union{Vector{Float64}, Nothing} = nothing
    heave::Union{Vector{Float64}, Nothing} = nothing
    surge::Union{Vector{Float64}, Nothing} = nothing
    yaw::Union{Vector{Float64}, Nothing} = nothing
    pitch::Union{Vector{Float64}, Nothing} = nothing
    roll::Union{Vector{Float64}, Nothing} = nothing
    step_duration::Union{Vector{Float64}, Nothing} = [1/mpc.cfg.rc.sampling_freq]
end

@kwdef struct sine_steps <: STEP_PARAM
    freq_acc::Float64
    peak_vel::Float64
end

function prepare_steps(step_param::sine_steps, Ts = 1/mpc.cfg.rc.sampling_freq)

    Tacc = 1/step_param.freq_acc

    t = collect(0:Ts:(Tacc-Ts))   # Time vector
    peak_acc = (step_param.peak_vel*pi)/(1/step_param.freq_acc)

    acc = peak_acc*sin.(2*pi*t./Tacc)    
    vel = peak_acc*Tacc/(2*pi)*(1 .-cos.(2*pi*t./Tacc))
    pos = peak_acc*Tacc/(2*pi)*(t-Tacc/(2*pi)*sin.(2*pi*t/Tacc))

    return pos
end

@kwdef struct conv_sine_steps <: STEP_PARAM
    freq_acc::Float64
    peak_vel::Float64
    cycles::Int
    kernel::String
end

function prepare_steps(step_param::conv_sine_steps)
    Fs = mpc.cfg.rc.sampling_freq                # Sampling frequency
    Ts = 1/Fs               # Sampling period
    Tacc = 1/step_param.freq_acc

    t = collect(0:Ts:Tacc*step_param.cycles)   # Time vector
    peak_acc = (step_param.peak_vel*pi)/(1/step_param.freq_acc) # this line was added by df who has no clue what a conv sine is, to make the code work
    pos = -peak_acc / (2*pi*1/Tacc)^2 * sin.(2*pi*t./Tacc)

    if step_param.kernel == "linear"
        a = size(pos,1)
        if mod(a,2) == 1
            f = (a-1)/2
            k = [collect(0:(1/f):1); collect(1-(1/f):-1/f:0)]
        else
            f = a/2
            k = [collect(0:(1/f):1); collect(1:-1/f:0)]
        end
        pos = pos .* k
    end

    if step_param.kernel == "gaussian"
        a = size(pos,1)
        if mod(a,2) == 1
            kernel = pdf.(Normal(0.0, 1.0),
                          collect(-3:2*3/(size(pos,1)-1):3))
        else
            kernel = pop!(pdf.(Normal(0.0, 1.0),
                               collect(-3:2*3/(size(pos,1)):3)))
            mpc.print_msg("Warning", "input nb_data was even. Obtained gaussian is not symmetric!")
        end

        # scale for max-value = 1
        kernel = kernel./maximum(kernel)
        pos = pos .* kernel
    end

    return pos
end

@kwdef struct noise_steps <: STEP_PARAM
    freq_acc::Float64
    noise::Float64
end

function prepare_steps(step_param::noise_steps, Ts = 1/mpc.cfg.rc.sampling_freq)
    Tacc = 1/step_param.freq_acc
    t = collect(0:Ts:(Tacc-Ts))   # Time vector

    pos = rand(Normal(0.0, step_param.noise), length(t))
    return pos
end

@kwdef struct linear_steps <: STEP_PARAM
    gradient::Float64 = 30.0
    duration::Float64 = 10.0
    sampling_freq::Float64 = mpc.cfg.rc.sampling_freq
end

@kwdef struct linear_vel_steps <: STEP_PARAM
    gradient::Float64 = 30.0
    duration::Float64 = 10.0
    sampling_freq::Float64 = mpc.cfg.rc.sampling_freq
    offset::Float64 = 0.0
    down::Bool = false
end

@kwdef struct const_vel_steps <: STEP_PARAM
    value::Float64
    duration::Float64 = 10.0
    offset::Float64 = 0.0
    sampling_freq::Float64 = mpc.cfg.rc.sampling_freq
end

function prepare_steps(step_param::linear_steps)
    srate = mpc.cfg.rc.sampling_freq
    pos = collect(1:1:step_param.duration*srate) .* (step_param.gradient/srate)
    return pos
end

function prepare_steps(step_param::linear_vel_steps)
    srate = mpc.cfg.rc.sampling_freq
    if step_param.down
        vel = collect(step_param.duration*srate:-1:1) .* (step_param.gradient/srate)
    else
        vel = collect(1:1:step_param.duration*srate) .* (step_param.gradient/srate)
    end


    if step_param.offset == 0.0
        return cumsum(vel)./srate
    else
        vel = cumsum(vel)./srate           
        return step_param.offset.+vel
    end
end

function prepare_steps(step_param::const_vel_steps)
    srate = mpc.cfg.rc.sampling_freq
   
    pos = [step_param.value + step_param.offset]
    for x = 1:step_param.duration*srate
        push!(pos, pos[end]+step_param.value)
    end

    return pos 
end


function seq_2_motion(start_position::pltf_position, 
                      steps::steps; 
                      exit_on::String = "motion", 
                      reverse::Bool = false, 
                      log::Bool = false, 
                      rot_center::position3d = mpc.position3d(0.0, 0.0, 0.0))

    dof_out = deepcopy(start_position)

    # get axis names
    fields = fieldnames(typeof(dof_out))

    # get length of supplied steps
    length_steps = nothing
    for field in fields
        if typeof(getfield(steps, field)) != Nothing
            length_steps = length(getfield(steps, field))
        end
    end

    # repeat dofs
    for field in fields
        value = getfield(dof_out, field)
        value = repeat([value], length_steps)
        setfield!(dof_out, field, value)
    end

    # add specified steps
    for axis in fields
        if typeof(getfield(steps, axis)) != Nothing
            axis_values = getfield(dof_out, axis)
            steps_values = getfield(steps, axis)
            values = axis_values .+ steps_values
            setfield!(dof_out, axis, values)
        end
    end

    motion = mpc.motion_template(dof = dof_out,
                                 duration = repeat(steps.step_duration, length_steps),
                                 law = "L",
                                 max_actuator_speed = 200,
                                 rotcenter = rot_center,
                                 exit_on = exit_on,
                                 reverse = reverse,
                                 log = log)
    return motion
end


