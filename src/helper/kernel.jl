# ===========================================================
#  Copyright (C) 2021 Gerda Wyssen, Daniel Fitze and Matthias Ertl, University of Bern,
#  matthias.ertl@unibe.ch
#  gerda.wyssen@unibe.ch 
#  daniel.fitze@unibe.ch 
# 
#  This file is part of mpc.
# 
#  mpc is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
# 
#  mpc is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with mpc. If not, see <http://www.gnu.org/licenses/>.
#  ===========================================================

function get_kernel(cfg_g::gaussian)
  if mod(cfg_g.nb_data,2) == 1 
    kernel = pdf.(Normal(cfg_g.shift,cfg_g.scale), 
                  collect(-3:2*cfg_g.r/cfg_g.nb_data:3))
  else
    kernel = pop!(pdf.(Normal(shift,cfg_g.scale), 
                       collect(-3:2*cfg_g.r/(cfg_g.nb_data+1):3)))
    println([ "Warning: Input nb_data was even. Obtained gaussian is not symmetric!"  ])
  end

  # scale for max-value = 1
  kernel = kernel./maximum(kernel)
  return kernel
end

function get_kernel(cfg_g::triangular)
  if mod(cfg_g.nb_data,2) == 1
    f = (cfg_g.nb_data-1)/2
    kernel = [collect(0:(1/f):1); collect(1:-1/f:0)]
  else
    f = (cfg_g.nb_data)/2
    kernel = [collect(0:(1/f):1); pop!(collect(1:-1/f:0))]
  end

  # scale for max-value = 1
  kernel =kernel./maximum(kernel)
  return kernel
end

function get_kernel(cfg_g::neutral)
  kernel = ones(cfg_g.nb_data)
  
  return kernel
end
