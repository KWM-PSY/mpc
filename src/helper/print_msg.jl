function print_msg(type::String, msg::Union{String, Nothing})
    isnothing(msg) ? msg = "Nothing" : msg = msg
    types = ["Status", "Topic", "Warning", "Timeout", "Timeout btn:", "Unknown"]
    length_max = maximum(length.(types))        
    gap_msg = length_max - length(type) + 1 
    gap_end = 20 - length(msg) 

    if type in ["Status", "Topic"]
        col = :green
    elseif type in ["Warning", "Unknown"]
        col = :red
    elseif type == "Timeout"
        col = :yellow
    end

    printstyled("MPC: ", color = :light_black, bold = true)
    printstyled("[ $type:", color = col)
    for i in 1:gap_msg print(" ") end
    print(msg)
    for i in 1:gap_end print(" ") end
    printstyled(" ]\n", color = col)
end
