function get_input(text, accepted_type = "string", accepted_values = [])

  while true
    print(text,": ")
    inp = readline()
    println(accepted_type)
        
    if lowercase(accepted_type) == "string"
      if ~isempty(accepted_values)
        tmp = findall(accepted_values -> accepted_values == inp,accepted_values)
        if ~isempty(tmp)
          return inp
        else
          println("only this inputs are allowed: ", accepted_values)
        end
      else
        return inp
      end
    elseif lowercase(accepted_type) == "int"
      try
        inp = parse(Int64, inp)
        if ~isempty(accepted_values)
          if inp >= accepted_values[1] && inp <= accepted_values[2]
            return inp
          else
            println("Input has to be in the range ", 
                    accepted_values[1],
                    " - ", 
                    accepted_values[2])
          end
        else
          return inp
        end
      catch
        println("")
        println("Input must be an integer!")
      end
    end
  end

end

