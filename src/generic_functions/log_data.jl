function log_data(;path, col_names, values)
	# compare length of col_names and values
	if length(col_names) != length(values)
		printstyled("[ INFO: ", color = :yellow)
		printstyled("number of values and cols do not match!")
		printstyled(" ]", color = :yellow)
	end
	# prepare csv strings
	COL_NAMES = join(col_names, ',')
	VALUES = join(values, ',')

	# write col names
	#   - check if document exists and is empty
	if !isfile(path)
		open(path, "a") do f
			write(f, COL_NAMES, "\n")
		end
	elseif isfile(path) && filesize(path) == 0
		open(path, "a") do f
			write(f, COL_NAMES, "\n")
		end
	end
	# write values
	open(path, "a") do f
		write(f, VALUES, "\n")
	end
end
