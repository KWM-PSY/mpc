# ===========================================================
#  Copyright (C) 2021 Gerda Wyssen, Daniel Fitze and Matthias Ertl, University of Bern,
#  matthias.ertl@unibe.ch
#  gerda.wyssen@unibe.ch 
#  daniel.fitze@unibe.ch 
# 
#  This file is part of mpc.
# 
#  mpc is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
# 
#  mpc is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with mpc. If not, see <http://www.gnu.org/licenses/>.
#  ===========================================================
function check_topic(required_topic::String)
  if required_topic == "*"
    return
  end
  mpc.sendmsg(mpc.MSG_GET_TOPIC, "")
  mpc.wait_for(type = mpc.sts,
               parameter = "topic",
               msg_ack = mpc.MSG_TOPIC,
               msg_nak = nothing,
               out_ack = nothing,
               out_nak = nothing)
  if mpc.sts.topic == required_topic
      if mpc.cfg.rc.disp_info
          mpc.print_msg("Topic", mpc.sts.topic)
      end
  else
      println("Wrong topic selected on server machine!")
      error("Expected $required_topic, found $(mpc.sts.topic)")
  end
end
