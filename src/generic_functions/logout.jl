# ===========================================================
#  Copyright (C) 2021 Gerda Wyssen, Daniel Fitze and Matthias Ertl, University of Bern,
#  matthias.ertl@unibe.ch
#  gerda.wyssen@unibe.ch 
#  daniel.fitze@unibe.ch 
# 
#  This file is part of mpc.
# 
#  mpc is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
# 
#  mpc is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with mpc. If not, see <http://www.gnu.org/licenses/>.
#  ===========================================================
function logout()
    # prepare target mail adress for logfile
    reportmsg = ""
    if(mpc.cfg.rc.mailto != nothing)
        reportmsg = string(mpc.cfg.rc.log_resolution)*","*mpc.cfg.rc.mailto
    end

    mpc.sendmsg(mpc.MSG_LOGOUT,reportmsg)

    mpc.wait_for(type = mpc.sts,
                 parameter = "logout_state",
                 msg_ack = mpc.MSG_LOGOUT_ACK,
                 msg_nak = mpc.MSG_LOGOUT_NAK,
                 out_ack = "logout acknowledged",
                 out_nak = "logout failed",
                 timeout = 30.0)

    mpc.sts.stop_loop = true
    println("=====> stop loop")
    sleep(2.0)
end
