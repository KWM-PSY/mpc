"""
checks if current position matches the requested position
"""
function check_position(pos::pltf_position)
    equal = true

    #println("------------------")
    #println("Checking position")
    for dof in fieldnames(typeof(pos))
        val_sts = round(getfield(mpc.sts.current_dof, dof)[1], digits = 2)
        val_pos = round(getfield(pos, dof)[1], digits = 2)
     #   @show dof, val_sts, val_pos

        if val_sts != val_pos
            equal = false
        end
    end
    #println("------------------")
    if !equal && mpc.cfg.rc.disp_info
        mpc.print_msg("Warning", "Pltf not at requested position!")
    end
    return equal
end


