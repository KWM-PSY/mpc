# ===========================================================
#  Copyright (C) 2022 Gerda Wyssen, Daniel Fitze, Matthias Ertl, University of Bern,
#  gerda.wyssen@unibe.ch, daniel.fitze@unibe.ch, matthias.ertl@unibe.ch
# 
#  This file is part of mpc.
# 
#  mpc is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
# 
#  mpc is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with mpc.  If not, see <http://www.gnu.org/licenses/>.
#  ===========================================================
function start_session(lab_config::LAB_CONFIG, input::input, output::output)
    global cfg = mpc.config(lab_config, input, output)

    mpc.load_pltf_const(lab_config)

    mpc.run_loop(cfg)
    mpc.login(cfg.output.motion)
    mpc.check_topic(lab_config.topic)
    mpc.engage()
    mpc.start_in_out(input, output)
end

function start_session(lab_config::LAB_CONFIG, params::feedback)
    global cfg = mpc.config_special(lab_config)

    mpc.load_pltf_const(lab_config)

    mpc.run_loop(cfg)
    mpc.login(params)
    mpc.check_topic(lab_config.topic)
end


