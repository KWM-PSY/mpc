# ===========================================================
#  Copyright (C) 2023 Matthias Ertl, University of Bern,
#  matthias.ertl@unibe.ch
#
#  This file is part of mpc.
# 
#  mpc is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
# 
#  mpc is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with mpc. If not, see <http://www.gnu.org/licenses/>.
#  ===========================================================
function bids_add(path2bids::String, allowed_sessions::Array{String}) 
    if ~isfile(path2bids * "participants.tsv")
        mkpath(path2bids)
        touch(path2bids * "participants.tsv")
        ptsv = open(path2bids * "participants.tsv","w")
        println(ptsv, "participant_id")
        Base.close(ptsv)
    end

    # load participants.tsv
    ptsv = CSV.read(path2bids * "participants.tsv", DataFrame)

    while true
        global ID = "test"
        global trialcounter = 0
        global date = string(Dates.format(Dates.now(), "yy-mm-dd"))
        global time_julia = string(Dates.format(Dates.now(), "HH-MM-SS"))

        println("----------------------------")
        if length(allowed_sessions) > 1
            global session = mpc.get_input("Enter session",
                                           "string",
                                           allowed_sessions)
        else
            global session = allowed_sessions[1] 
        end

        if session == allowed_sessions[1] 
            # create unique participant_id
            while true
                global vp = randstring("ABCDEFGHIJKLMNOPQRSTUVW",5)
                
                # check if string is unique and add to file
                if isempty(findall( x -> x == vp, ptsv.participant_id))
                    ptsv = open(path2bids * "participants.tsv", "a")
                    println(ptsv, vp)
                    Base.close(ptsv)
                    break
                end
            end
        else
            global vp = mpc.get_input("Enter participants_id",
                                      "string", ptsv.participant_id)
        end
        println("============================")
        @show ID
        @show vp
        @show session
        println("============================")
        println("Is this information correct [y/n]?")
        input = readline()
        if input == "y"
            break
        end
    end
    
    return vp, session
end


function create_bids(project::String, sub::String, task::String, sess::Union{String,Nothing}, type::String, add_ses = false)

created_struct = false

subid = "sub-" * sub
sesid = "ses-" * sess
taskid = "task-" * task
if typeof(sess) == Nothing
    fpath = "../../data/" * project * "/sub-" * sub * "/"
    fname = subid * "_" * taskid * "_" * type * ".tsv"
elseif typeof(sess) == String
    fpath = "../../data/" * project * "/sub-" * sub * "/ses-" * sess * "/"
    fname = subid * "_" * sesid * "_"* taskid * "_" * type * ".tsv"
end

# check if file already exist
if isfile(fpath * fname)
  laf = "../../data/lost&fond/"
  mkpath(laf)
  cname = subid * "_" * taskid * "_" * type * "_" * string(now()) * ".tsv"
 
  mv(fpath * fname, laf * cname)
end

mkpath(fpath)
io = open(fpath * fname, "w")
Base.close(io)

return fpath * fname 
end

function bids_write_header(file::String, input::Array{String})
    fh = open(file, "a")
    
    for x = 1:length(input) 
        msg = input[x]
        if x < length(input)
            print(fh, "$msg\t")
        else
            println(fh, "$msg")
        end
    end
    println("")
    Base.close(fh)
end
