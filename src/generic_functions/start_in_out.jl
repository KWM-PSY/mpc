function start_unit(unit::SESSION_TYPE)
    # do nothing because we need to login before starting the rest
end

function start_unit(unit::likert)
    mpc.sts.input_state.likert_state = unit
    mpc.start_unit(mpc.gamepad(btn_nb = 3,
                               btn_names = ["fl1", "fr1", "a"]))
end

function start_unit(unit::audio_srv)

    mpc.sendmsg(mpc.MSG_AV_OPEN_AUDIO, 
                @sprintf("%s", unit.audio_dir))

    mpc.wait_for(type = mpc.sts.output_state, 
                 parameter = "audio_state",
                 msg_ack = mpc.MSG_AV_AUDIO_ACK, 
                 msg_nak = mpc.MSG_AV_AUDIO_NAK,
                 out_ack = "opend audio player",
                 out_nak = "couln't open audio player")
end

function start_unit(unit::gamepad)
    mpc.sendmsg(mpc.MSG_ACTIVATE_HW_INPUT,
                @sprintf("%s|%s|%s", 
                         "gamepad", # name
                         "gamepad", # tag
                         join(unit.btn_names, "|")))

    mpc.wait_for(type = mpc.sts.input_state, 
                 parameter = "gamepad_state",
                 msg_ack = mpc.MSG_HW_INPUT_ACK, 
                 msg_nak = mpc.MSG_HW_INPUT_NAK,
                 out_ack = "gamepad connected",
                 out_nak = "couldn't connect gamepad")
end

function start_unit(unit::img_srv)
    screens = getfield(unit, :displays)
    msg = join(mpc.prepare_disp_msg.(screens), "|")

    mpc.sendmsg(mpc.MSG_AV_OPEN_GLSTILLS, 
                @sprintf("%s,%s", unit.img_dir, msg))

    mpc.wait_for(type = mpc.sts.output_state, 
                 parameter = "img_state",
                 msg_ack = mpc.MSG_AV_GLW_ACK, 
                 msg_nak = mpc.MSG_AV_GLW_NAK,
                 out_ack = "opend img srv",
                 out_nak = "couldn't open img srv")
    mpc.wait_for(type = mpc.sts.output_state, 
                 parameter = "img_state",
                 msg_ack = mpc.MSG_AV_GLSTILLS_SERVER_READY, 
                 msg_nak = mpc.MSG_AV_GLW_NAK,
                 out_ack = "img srv ready",
                 out_nak = "img srv is not ready")
end


function start_unit(unit::transducer_srv)
end

function start_unit(unit::btn)
    # this function is required to be able to loop over all outputs
    # it does nothing
end

function start_in_out(input::input, output::output)

    # start in
    for field in fieldnames(typeof(input))
        val = getfield(input, field)
        if !isnothing(val)
            mpc.start_unit(val)
        end
    end
    # start out
    for field in fieldnames(typeof(output))
        val = getfield(output, field)
        if !isnothing(val)
            mpc.start_unit(val)
        end
    end
end


