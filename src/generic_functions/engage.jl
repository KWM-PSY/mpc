# ===========================================================
#  Copyright (C) 2021 Gerda Wyssen, Daniel Fitze and Matthias Ertl, University of Bern,
#  matthias.ertl@unibe.ch
#  gerda.wyssen@unibe.ch 
#  daniel.fitze@unibe.ch 
# 
#  This file is part of mpc.
# 
#  mpc is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
# 
#  mpc is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with mpc. If not, see <http://www.gnu.org/licenses/>.
#  ===========================================================
#
function engage()
    if mpc.cfg.rc.pltf_model == "Moog_1"
        if mpc.sts.pltf_state != mpc.PLTF_STATE_IDLE
            mpc.print_msg("Warning", "must be in IDLE-state in order to engage")
        end
    elseif mpc.cfg.rc.pltf_model == "Moog_2"
        if mpc.sts.pltf_state != mpc.PLTF2_STATE_READY_FOR_TRAIN
            mpc.print_msg("Warning", "must be in IDLE-state in order to engage")
        end
    elseif mpc.cfg.rc.pltf_model == "Yaw_2"
        if mpc.sts.pltf_state != mpc.PLTFYAW_STATE_CONNECTED
            mpc.print_msg("Warning", "must be in IDLE-state in order to engage")
        end
    else
        mpc.print_msg("Warning", "unsupported pltf model")
    end

    mpc.sendmsg(mpc.MSG_ENGAGE,"")

    # SHORT WAITING PERIOD DUE TO STRANGE BEHAVIOR WITH Yaw_2
    # - MSG_ENGAGE_ACK is somehow missed
    #
    # WORKS WITH MOOG 1 & 2
    #
    # !!!!!!!!!!! SOMETIMES IT WORKS HOWEVER THE FUCK WHO KNOWS !!!!!
    #
    # wait for engage ACK
    mpc.wait_for(type = mpc.sts,
                 parameter = "engage_state",
                 msg_ack = mpc.MSG_ENGAGE_ACK,
                 msg_nak = mpc.MSG_ENGAGE_NAK,
                 out_ack = "engage acknowledged",
                 out_nak = "engage failed",
                 timeout = 1.0)

    # wait for engage DONE
    mpc.wait_for(type = mpc.sts,
                 parameter = "engage_state",
                 msg_ack = mpc.MSG_ENGAGE_DONE,
                 msg_nak = mpc.MSG_ENGAGE_NAK,
                 out_ack = "engage done",
                 out_nak = "engage failed",
                 timeout = 30.0)
end

