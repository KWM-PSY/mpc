# ===========================================================
#  Copyright (C) 2021 Gerda Wyssen, Daniel Fitze and Matthias Ertl, University of Bern,
#  matthias.ertl@unibe.ch
#  gerda.wyssen@unibe.ch 
#  daniel.fitze@unibe.ch 
# 
#  This file is part of mpc.
# 
#  mpc is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
# 
#  mpc is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with mpc. If not, see <http://www.gnu.org/licenses/>.
#  ===========================================================

function park()
    if mpc.cfg.rc.pltf_model == "Moog_1"
        if mpc.sts.pltf_state != mpc.PLTF_STATE_ENGAGED
            mpc.print_msg("Warning", "must be in ENGAGED-state in order to park")
        end
    elseif mpc.cfg.rc.pltf_model == "Moog_2"
        if mpc.sts.pltf_state != mpc.PLTF2_STATE_ENGAGED
            mpc.print_msg("Warning", "must be in ENGAGED-state in order to park")
        end
    else
        mpc.print_msg("Warning", "unsuported pltf model")
    end
    mpc.sendmsg(mpc.MSG_PARK,"")

    mpc.wait_for(type = mpc.sts,
                 parameter = "park_state",
                 msg_ack = mpc.MSG_PARK_ACK,
                 msg_nak = mpc.MSG_PARK_NAK,
                 out_ack = "park acknowledged",
                 out_nak = "park failed",
                 timeout = 30.0)

    mpc.wait_for(type = mpc.sts,
                 parameter = "park_state",
                 msg_ack = mpc.MSG_PARK_DONE,
                 msg_nak = mpc.MSG_PARK_NAK,
                 out_ack = "park done",
                 out_nak = "park failed",
                 timeout = 30.0)
end

function park(type::feedback)
    println("yes I am here")
    if mpc.cfg.rc.pltf_model == "Moog_1"
        if mpc.sts.pltf_state != mpc.PLTF_STATE_ENGAGED
            mpc.print_msg("Warning", "must be in ENGAGED-state in order to park")
        end
    elseif mpc.cfg.rc.pltf_model == "Moog_2"
        if mpc.sts.pltf_state != mpc.PLTF2_STATE_ENGAGED
            mpc.print_msg("Warning", "must be in ENGAGED-state in order to park")
        end
    elseif mpc.cfg.rc.pltf_model == "Yaw_2"

    else
        mpc.print_msg("Warning", "unsuported pltf model: $(mpc.cfg.rc.pltf_model)")
    end
    mpc.sendmsg(mpc.MSG_FEEDBACK_PARK,"")

    mpc.wait_for(type = mpc.sts,
                 parameter = "park_state",
                 msg_ack = mpc.MSG_PARK_ACK,
                 msg_nak = mpc.MSG_PARK_NAK,
                 out_ack = "park acknowledged",
                 out_nak = "park failed",
                 timeout = 30.0)

    mpc.wait_for(type = mpc.sts,
                 parameter = "park_state",
                 msg_ack = mpc.MSG_PARK_DONE,
                 msg_nak = mpc.MSG_PARK_NAK,
                 out_ack = "park done",
                 out_nak = "park failed",
                 timeout = 30.0)
end

