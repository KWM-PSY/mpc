function get_log()
    tag = mpc.sts.session_tag
    path = "/platf_base/sessions/20"*tag[1:2]*"/"*tag[3:4]*"/"*tag[5:6]*"/"*tag[8:13]

    cmd = `rsync -avux moog@$(mpc.cfg.rc.server_ip):$path $(mpc.cfg.rc.path_data)`
    run(cmd)
end
