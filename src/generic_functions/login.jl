# ===========================================================
#  Copyright (C) 2022 Gerda Wyssen, Daniel Fitze and Matthias Ertl, University of Bern,
#  matthias.ertl@unibe.ch
#  gerda.wyssen@unibe.ch 
#  daniel.fitze@unibe.ch 
# 
#  This file is part of mpc.
# 
#  mpc is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
# 
#  mpc is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with mpc. If not, see <http://www.gnu.org/licenses/>.
#  ===========================================================
function login(params::short; do_engage::Bool = true)

    if mpc.cfg.input.btn != nothing
        srv_btn = join(mpc.cfg.input.btn.btn_labels, ",")
    else
        srv_btn = ""
    end

    # SEND LOGIN MSG
    mpc.sendmsg(mpc.MSG_LOGIN, 
                @sprintf("%d,%d,%s,%f,%s", 
                         mpc.SESSION_SHORT_SEQ, 
                         mpc.sts.udp_port.remport, 
                         params.actuator_mode, 
                         params.start_height, 
                         srv_btn))

    # wait for login ACK
    mpc.wait_for(type = mpc.sts,
                 parameter = "login_state", 
                 msg_ack = mpc.MSG_LOGIN_ACK, 
                 msg_nak = mpc.MSG_LOGIN_NAK,
                 out_ack = "login acknowledged",
                 out_nak = "login failed",
                 timeout = 30.0)

    if mpc.cfg.rc.pltf_model == "Moog_1"
        # wait for IDLE
        mpc.wait_for(type = mpc.sts,
                     parameter = "pltf_state", 
                     msg_ack = mpc.PLTF_STATE_IDLE, 
                     msg_nak = mpc.MSG_LOGIN_NAK,
                     out_ack = "pltf ready",
                     out_nak = "something went wrong",
                     timeout = 30.0)
    elseif mpc.cfg.rc.pltf_model == "Moog_2"
        # wait for READY FOR TRAIN
        mpc.wait_for(type = mpc.sts,
                     parameter = "pltf_state", 
                     msg_ack = mpc.PLTF2_STATE_READY_FOR_TRAIN, 
                     msg_nak = mpc.MSG_LOGIN_NAK,
                     out_ack = "pltf ready",
                     out_nak = "something went wrong",
                     timeout = 30.0)
    elseif mpc.cfg.rc.pltf_model == "Yaw_2"
        # wait for READY FOR TRAIN
        
        println("Waiting fo Connected")
        mpc.wait_for(type = mpc.sts,
                     parameter = "pltf_state",
                     msg_ack = mpc.PLTFYAW_STATE_CONNECTED,
                     msg_nak = mpc.MSG_LOGIN_NAK,
                     out_ack = "pltf connected",
                     out_nak = "something went wrong",
                     timeout = 90.0)

        println("Set Reference")
        mpc.yaw_set_ref_pos()
        

    else
        mpc.print_msg("Warning", "unsuported pltf model")
    end
end

function login(params::feedback; do_engage::Bool = true)

    println(@sprintf("%d,%d,%s,%s,%f,%s", 
                         mpc.SESSION_FEEDBACK, 
                         mpc.sts.udp_port.remport, 
                         params.active_joystick,
                         params.axes_spec, 
                         params.delay,
                         params.smoothing_type*string(params.smoothing_duration)))


    # SEND LOGIN MSG
    mpc.sendmsg(mpc.MSG_LOGIN, 
                @sprintf("%d,%d,%s,%s,%f,%s", 
                         mpc.SESSION_FEEDBACK, 
                         mpc.sts.udp_port.remport, 
                         params.active_joystick,
                         params.axes_spec, 
                         params.delay,
                         params.smoothing_type*string(params.smoothing_duration)))
                        

    # wait for login ACK
    mpc.wait_for(type = mpc.sts,
                 parameter = "login_state", 
                 msg_ack = mpc.MSG_LOGIN_ACK, 
                 msg_nak = mpc.MSG_LOGIN_NAK,
                 out_ack = "login acknowledged",
                 out_nak = "login failed",
                 timeout = 30.0)

    if mpc.cfg.rc.pltf_model == "Moog_1"
        # wait for IDLE
        mpc.wait_for(type = mpc.sts,
                     parameter = "pltf_state", 
                     msg_ack = mpc.PLTF_STATE_IDLE, 
                     msg_nak = mpc.MSG_LOGIN_NAK,
                     out_ack = "pltf ready",
                     out_nak = "something went wrong",
                     timeout = 30.0)
    elseif mpc.cfg.rc.pltf_model == "Moog_2"
        # wait for READY FOR TRAIN
        mpc.wait_for(type = mpc.sts,
                     parameter = "pltf_state", 
                     msg_ack = mpc.PLTF2_STATE_READY_FOR_TRAIN, 
                     msg_nak = mpc.MSG_LOGIN_NAK,
                     out_ack = "pltf ready",
                     out_nak = "something went wrong",
                     timeout = 30.0)
    elseif mpc.cfg.rc.pltf_model == "Yaw_2"
        # wait for READY FOR TRAIN
        println("Waiting fo Connected")
        mpc.wait_for(type = mpc.sts,
                     parameter = "pltf_state",
                     msg_ack = mpc.PLTFYAW_STATE_CONNECTED,
                     msg_nak = mpc.MSG_LOGIN_NAK,
                     out_ack = "pltf connected",
                     out_nak = "something went wrong",
                     timeout = 90.0)
    else
        mpc.print_msg("Warning", "unsuported pltf model")
    end
end

function login(params::platformfree; do_engage::Bool = true)
  if mpc.cfg.input.btn!=nothing
    srv_btn=join(mpc.cfg.input.btn.btn_labels,",")
  else
    srv_btn = ""
  end

  # SEND LOGIN MSG
  mpc.sendmsg(mpc.MSG_LOGIN,
              @sprintf("%d,%d,%s",
                       mpc.SESSION_NOPLATFORM,
                       mpc.sts.udp_port.remport,
                       srv_btn))

  # wait for login ACK
  mpc.wait_for(type = mpc.sts,
               parameter = "login_state",
               msg_ack = mpc.MSG_LOGIN_ACK,
               msg_nak = mpc.MSG_LOGIN_NAK,
               out_ack = "login acknowledged",
               out_nak = " login failed",
               timeout = 30.0)

end

function login(params::immediate_vel; do_engage::Bool = true)


    # SEND LOGIN MSG
    mpc.sendmsg(mpc.MSG_LOGIN,
                @sprintf("%d,%d,%s,%f,%s",
                         mpc.SESSION_IMMEDIATE_VEL,
                         mpc.sts.udp_port.remport,
                         params.max_diff_lin,
                         params.max_diff_rot,
                         600))

    # wait for login ACK
    mpc.wait_for(type = mpc.sts,
                 parameter = "login_state", 
                 msg_ack = mpc.MSG_LOGIN_ACK, 
                 msg_nak = mpc.MSG_LOGIN_NAK,
                 out_ack = "login acknowledged",
                 out_nak = "login failed",
                 timeout = 30.0)

    if mpc.cfg.rc.pltf_model == "Moog_1"
        # wait for IDLE
        mpc.wait_for(type = mpc.sts,
                     parameter = "pltf_state", 
                     msg_ack = mpc.PLTF_STATE_IDLE, 
                     msg_nak = mpc.MSG_LOGIN_NAK,
                     out_ack = "pltf ready",
                     out_nak = "something went wrong",
                     timeout = 30.0)
    elseif mpc.cfg.rc.pltf_model == "Moog_2"
        # wait for READY FOR TRAIN
        mpc.wait_for(type = mpc.sts,
                     parameter = "pltf_state", 
                     msg_ack = mpc.PLTF2_STATE_READY_FOR_TRAIN, 
                     msg_nak = mpc.MSG_LOGIN_NAK,
                     out_ack = "pltf ready",
                     out_nak = "something went wrong",
                     timeout = 30.0)
    elseif mpc.cfg.rc.pltf_model == "Yaw_2"
        # wait for READY FOR TRAIN
        println("Waiting fo Connected")
        mpc.wait_for(type = mpc.sts,
                     parameter = "pltf_state",
                     msg_ack = mpc.PLTFYAW_STATE_CONNECTED,
                     msg_nak = mpc.MSG_LOGIN_NAK,
                     out_ack = "pltf connected",
                     out_nak = "something went wrong",
                     timeout = 90.0)
    else
        mpc.print_msg("Warning", "unsuported pltf model")
    end
end


@testset "login" begin
    println("test not written yet")
end

