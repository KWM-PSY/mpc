# ===========================================================
#  Copyright (C) 2022 Gerda Wyssen, Daniel Fitze and Matthias Ertl, University of Bern,
#  matthias.ertl@unibe.ch
#  gerda.wyssen@unibe.ch 
#  daniel.fitze@unibe.ch 
# 
#  This file is part of mpc.
# 
#  mpc is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
# 
#  mpc is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with mpc. If not, see <http://www.gnu.org/licenses/>.
#  ===========================================================
function get_input(text::String, accepted_values::Vector{String})

  while true
    print(text,": ")
    inp = readline()
    if ~isempty(accepted_values)
      tmp = findall(accepted_values -> accepted_values == inp,accepted_values)
      if ~isempty(tmp)
        return inp
      else
        println("[  Only these inputs are allowed: $accepted_values  ]")
      end
    else
      return inp
    end
  end
end


function get_input(text::String, accepted_values::Vector{T}) where {T} 

  while true
    print(text,": ")
    inp = readline()
   
    go = false
    try
      inp = parse(T, inp)
      go = true
    catch
      println("Can't parse input to type $T")
    end

    if ~isempty(accepted_values) && go
      if inp >= accepted_values[1] && inp <= accepted_values[2]
        return inp
      else
        println("[  Input has to be in the range ", 
                accepted_values[1],
                " - ", 
                accepted_values[2], 
                "  ]")
      end
    end
  end
end

