function end_session(;get_session_data = false)
    mpc.park()

    mpc.logout()

    mpc.sts.stop_loop = true

    if get_session_data
        println("get log file per scp")
        mpc.get_log()
    end

    mpc.close(mpc.sts.udp_port)

    global cfg = nothing
    global sts = nothing
end

