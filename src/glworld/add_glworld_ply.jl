# ===========================================================
#  Copyright (C) 2022 Gerda Wyssen, Daniel Fitze and Matthias Ertl, University of Bern,
#  matthias.ertl@unibe.ch
#  gerda.wyssen@unibe.ch 
#  daniel.fitze@unibe.ch 
# 
#  This file is part of mpc.
# 
#  mpc is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
# 
#  mpc is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with mpc. If not, see <http://www.gnu.org/licenses/>.
#  ===========================================================
function add_glworld_ply(cfg_session::cfg_session, cfg_ply::ply)

	mg = @sprintf("%s,%s,%f,%d,%d,%d,%d,%d,%d,%d,%d,%d,%f", 
                cfg_ply.name, 
                cfg_ply.file,
                cfg_ply.scale,
                cfg_ply.ambient.r, 
                cfg_ply.ambient.g, 
                cfg_ply.ambient.b, 
                cfg_ply.diffuse.r, 
                cfg_ply.diffuse.g, 
                cfg_ply.diffuse.b, 
                cfg_ply.specular.r, 
                cfg_ply.specular.g, 
                cfg_ply.specular.b, 
                cfg_ply.shine, 
                cfg_ply.radius, 
                cfg_ply.refinements)
  
  mpc_session, recv = mpc.srv_resp(cfg_session, 
                                   mpc.MSG_AV_GLW_ACK, 
                                   mpc.MSG_AV_GLW_NAK,
                                   "[  Added ply object $cfg_ply.name  ]",
                                   "[  Something went wrong  ]")
  
  return cfg_session
end

function add_glworld_primitive(cfg_session::cfg_session, cfg_prim::gl_cylinder)

	mg = @sprintf("%s,%s,%d,%d,%d,%d,%d,%d,%d,%d,%d,%f,%f,%f,%d", 
                cfg_prim.name, 
                "CYL", 
                cfg_prim.ambient.r, 
                cfg_prim.ambient.g, 
                cfg_prim.ambient.b, 
                cfg_prim.diffuse.r, 
                cfg_prim.diffuse.g, 
                cfg_prim.diffuse.b, 
                cfg_prim.specular.r, 
                cfg_prim.specular.g, 
                cfg_prim.specular.b, 
                cfg_prim.shine, 
                cfg_prim.radius,
                cfg_prim.length,
                cfg_prim.refinements)
  
  mpc_session, recv = mpc.srv_resp(cfg_session, 
                                   mpc.MSG_AV_GLW_ACK, 
                                   mpc.MSG_AV_GLW_NAK,
                                   "[  Added cylinder $cfg_prim.name  ]",
                                   "[  Something went wrong  ]")

  return cfg_session
end

function add_glworld_primitive(cfg_session::cfg_session, cfg_prim::gl_torus)

	mg = @sprintf("%s,%s,%d,%d,%d,%d,%d,%d,%d,%d,%d,%f,%f,%f,%d", 
                cfg_prim.name, 
                "TOR", 
                cfg_prim.ambient.r, 
                cfg_prim.ambient.g, 
                cfg_prim.ambient.b, 
                cfg_prim.diffuse.r, 
                cfg_prim.diffuse.g, 
                cfg_prim.diffuse.b, 
                cfg_prim.specular.r, 
                cfg_prim.specular.g, 
                cfg_prim.specular.b, 
                cfg_prim.shine, 
                cfg_prim.radius,
                cfg_prim.length,
                cfg_prim.refinements)
  
  mpc_session, recv = mpc.srv_resp(cfg_session, 
                                   mpc.MSG_AV_GLW_ACK, 
                                   mpc.MSG_AV_GLW_NAK,
                                   "[  Added torus $cfg_prim.name  ]",
                                   "[  Something went wrong  ]")
  
  return cfg_session
end

function add_glworld_primitive(cfg_session::cfg_session, cfg_prim::gl_tetrahedron)

	mg = @sprintf("%s,%s,%d,%d,%d,%d,%d,%d,%d,%d,%d,%f,%f", 
                cfg_prim.name, 
                "TET", 
                cfg_prim.ambient.r, 
                cfg_prim.ambient.g, 
                cfg_prim.ambient.b, 
                cfg_prim.diffuse.r, 
                cfg_prim.diffuse.g, 
                cfg_prim.diffuse.b, 
                cfg_prim.specular.r, 
                cfg_prim.specular.g, 
                cfg_prim.specular.b, 
                cfg_prim.shine, 
                cfg_prim.length)
  
  mpc_session, recv = mpc.srv_resp(cfg_session, 
                                   mpc.MSG_AV_GLW_ACK, 
                                   mpc.MSG_AV_GLW_NAK,
                                   "[  Added tetrahedron $cfg_prim.name  ]",
                                   "[  Something went wrong  ]")
 
  return cfg_session
end

function add_glworld_primitive(cfg_session::cfg_session, cfg_prim::gl_parallelepiped)

	mg = @sprintf("%s,%s,%d,%d,%d,%d,%d,%d,%d,%d,%d,%f,%f,%f,%f", 
                cfg_prim.name, 
                "PAR", 
                cfg_prim.ambient.r, 
                cfg_prim.ambient.g, 
                cfg_prim.ambient.b, 
                cfg_prim.diffuse.r, 
                cfg_prim.diffuse.g, 
                cfg_prim.diffuse.b, 
                cfg_prim.specular.r, 
                cfg_prim.specular.g, 
                cfg_prim.specular.b, 
                cfg_prim.shine, 
                cfg_prim.widht,
                cfg_prim.length,
                cfg_prim.depth)
  
  mpc_session, recv = mpc.srv_resp(cfg_session, 
                                   mpc.MSG_AV_GLW_ACK, 
                                   mpc.MSG_AV_GLW_NAK,
                                   "[  Added parallelepiped $cfg_prim.name  ]",
                                   "[  Something went wrong  ]")
  
  return cfg_session
end



