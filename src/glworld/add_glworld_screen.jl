# ===========================================================
#  Copyright (C) 2022 Gerda Wyssen, Daniel Fitze and Matthias Ertl, University of Bern,
#  matthias.ertl@unibe.ch
#  gerda.wyssen@unibe.ch 
#  daniel.fitze@unibe.ch 
# 
#  This file is part of mpc.
# 
#  mpc is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
# 
#  mpc is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with mpc. If not, see <http://www.gnu.org/licenses/>.
#  ===========================================================
function add_glworld_screen(cfg_session::session_def, cfg_screen::virtual_screen)

  if typeof(cfg_screen) == flat
    mg = @sprintf("%s,%d,%d,%d,%d,%s,%f,%f", 
                  cfg_screen.name, 
                  cfg_screen.resolution.width, 
                  cfg_screen.resolution.height, 
                  cfg_screen.image, 
                  cfg_screen.movie, 
                  "F", 
                  cfg_screen.dimension.x, 
                  cfg_screen.dimension.y)
  end

  mpc.sendmsg(cfg_session, mpc.MSG_AV_GLW_ADD_SCREEN, mg)

  sname = cfg_screen.name
  cfg_session, recv = mpc.srv_resp(cfg_session, 
                                   mpc.MSG_AV_GLW_ACK, 
                                   mpc.MSG_AV_GLW_NAK, 
                                   "[  Added screen $sname  ]", 
                                   "[  Something went wrong]")

  mpc.set_glworld_screen_color(cfg_session, 
                               cfg_screen, 
                               mpc.white())
  return cfg_session
end

function set_glworld_screen_color(cfg_session, cfg_screen::virtual_screen, c::def_color = mpc.white())
  mg = @sprintf("%s,%d,%d,%d", 
                cfg_screen.name, 
                c.r, 
                c.g, 
                c.b)
  
  mpc.sendmsg(cfg_session, 
              mpc.MSG_AV_GLW_RGB, 
              mg)
  
  return cfg_session
end
