# ===========================================================
#  Copyright (C) 2022 Gerda Wyssen, Daniel Fitze and Matthias Ertl, University of Bern,
#  matthias.ertl@unibe.ch
#  gerda.wyssen@unibe.ch 
#  daniel.fitze@unibe.ch 
# 
#  This file is part of mpc.
# 
#  mpc is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
# 
#  mpc is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with mpc. If not, see <http://www.gnu.org/licenses/>.
#  ===========================================================
function create_sequence(cfg_session::cfg_session, seq::sequence)
  
  mg = @sprintf("%s,%f,%d", 
                seq.name, 
                seq.duration, 
                seq.nb_object)


  mpc.sendmsg(cfg_session, 
              mpc.AV_GLW_SEQ_CREATE,
              mg)

  mpc_session, recv = mpc.srv_resp(cfg_session, 
                                   mpc.MSG_AV_GLW_ACK, 
                                   mpc.MSG_AV_GLW_NAK, 
                                   "[  Start creating sequence  ]",
                                   "[  Something went wrong  ]")

  return cfg_session
end
