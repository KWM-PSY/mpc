# ===========================================================
#  Copyright (C) 2022 Gerda Wyssen, Daniel Fitze and Matthias Ertl, University of Bern,
#  matthias.ertl@unibe.ch
#  gerda.wyssen@unibe.ch 
#  daniel.fitze@unibe.ch 
# 
#  This file is part of mpc.
# 
#  mpc is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
# 
#  mpc is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with mpc. If not, see <http://www.gnu.org/licenses/>.
#  ===========================================================
function set_screen_string(cfg_session::session_def, cfg_string::glw_string)

  utf_text = encode(cfg_string.text, 
                    "UTF-8")

  text_mg = ""
  for x = 1:size(utf_text,1)
    text_mg = text_mg * string(utf_text[x], base = 16)
  end

  mg = string(cfg_string.color.r) * "," * string(cfg_string.color.g) *  "," * string(cfg_string.color.b) *  "," * string(cfg_string.fontsize) *  "," * string(cfg_string.position.x) *  "," * string(cfg_string.position.y) *  "," * text_mg


  mpc.sendmsg(cfg_session, 
              mpc.MSG_AV_GLW_SCREEN_STRINGS, 
              mg)

  mpc_session, recv = mpc.srv_resp(cfg_session, 
                                   mpc.MSG_AV_GLW_ACK, 
                                   mpc.MSG_AV_GLW_NAK, 
                                   "[  Display string $image  ]",
                                   "[  Something went wrong  ]")
  
  return cfg_session
end
