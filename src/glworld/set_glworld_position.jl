# ===========================================================
#  Copyright (C) 2022 Gerda Wyssen, Daniel Fitze and Matthias Ertl, University of Bern,
#  matthias.ertl@unibe.ch
#  gerda.wyssen@unibe.ch 
#  daniel.fitze@unibe.ch 
# 
#  This file is part of mpc.
# 
#  mpc is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
# 
#  mpc is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with mpc. If not, see <http://www.gnu.org/licenses/>.
#  ===========================================================
function set_glworld_position(cfg_session::session_def, cfg_object::world_object)
  mg = @sprintf("%s,%d,%d,%d,%d,%d,%d", 
                cfg_object.name, 
                cfg_object.position.x, 
                cfg_object.position.y, 
                cfg_object.position.z, cfg_object.orientation.yaw, cfg_object.orientation.pitch, cfg_object.orientation.roll)
  mpc.sendmsg(cfg_session, mpc.MSG_AV_GLW_REPOSITION_NODE, mg)
 
  cobject = cfg_object.name
  cfg_session, recv = mpc.srv_resp(cfg_session, mpc.MSG_AV_GLW_ACK, mpc.MSG_AV_GLW_NAK, "[  Repositioned $cobject  ]", "Xomething went wrong")

  return cfg_session
end

function set_glworld_position(cfg_session::session_def, cfg_object::light_source)
  mg = @sprintf("%s,%d,%d,%d,%d,%d,%d", 
                cfg_object.name, 
                cfg_object.position.x, 
                cfg_object.position.y, 
                cfg_object.position.z, 
                0, 
                0, 
                0)
  mpc.sendmsg(cfg_session, 
              mpc.MSG_AV_GLW_REPOSITION_NODE, 
              mg)

  cfg_session, recv = mpc.srv_resp(cfg_session, 
                                   mpc.MSG_AV_GLW_ACK, 
                                   mpc.MSG_AV_GLW_NAK, 
                                   "[  Positioning successfull  ]", "[  Something went wrong  ]")
  
  return cfg_session
end

function set_glworld_position(cfg_session::session_def, cfg_object::camera)
  mg = @sprintf("%s,%d,%d,%d", cfg_object.name, cfg_object.position.x, cfg_object.position.y, cfg_object.position.z)
  mpc.sendmsg(cfg_session, mpc.MSG_AV_GLW_REPOSITION_NODE, mg)

  return cfg_session
end
