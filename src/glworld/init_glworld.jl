# ===========================================================
#  Copyright (C) 2022 Gerda Wyssen, Daniel Fitze and Matthias Ertl, University of Bern,
#  matthias.ertl@unibe.ch
#  gerda.wyssen@unibe.ch 
#  daniel.fitze@unibe.ch 
# 
#  This file is part of mpc.
# 
#  mpc is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
# 
#  mpc is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with mpc. If not, see <http://www.gnu.org/licenses/>.
#  ===========================================================
function init_glworld(cfg_session::session_def, cfg_world::Array{world_object})
  cfg_session = mpc.open_glworld(cfg_session, cfg_world)

  # add world objects
  for x = 1:length(cfg_world.objects)
    if typeof(cfg_world.objects[x]) == flat
      cfg_session = add_glworld_screen(cfg_session, 
                                       cfg_world.objects[x])
    
    elseif typeof(cfg_world.objects[x]) == camera
      cfg_session = mpc.set_camera_glworld(cfg_session, 
                                           cfg_world.objects[x])
      cfg_session = mpc.set_fov_glworld(cfg_session, 
                                        cfg_world.objects[x])

    elseif typeof(cfg_world.objects[x]) == light_source
      cfg_session = add_glworld_light(cfg_session, 
                                      cfg_world.objects[x])

    elseif typeof(cfg_world.objects[x]) == primitive 
      cfg_session = add_glworld_primitive(cfg_session, 
                                          cfg_world.objects[x])

    end
  end
  
  cfg_session = mpc.activate_glworld(cfg_session)

  # setup further properties of objects. this can only be done after the world
  # is activated
  for x = 1:length(cfg_world.objects)
    if typeof(cfg_world.objects[x]) == flat
      cfg_session = mpc.set_glworld_position(cfg_session, 
                                             cfg_world.objects[x])
      cfg_session = mpc.set_screen_color(cfg_session, 
                                         cfg_world.objects[x].name)
      cfg_session = mpc.set_visible_glworld(cfg_session, 
                                            cfg_world.objects[x].name)

    
    elseif typeof(cfg_world.objects[x]) == camera
#      cfg_session = mpc.set_fov_glworld(cfg_session, cfg_world.objects[x])
#      println("[  Setup camera  ]")

    elseif typeof(cfg_world.objects[x]) == primitive 
      cfg_session = mpc.set_glworld_position(cfg_session, 
                                             cfg_world.objects[x])
      cfg_session = mpc.set_visible_glworld(cfg_session, 
                                            cfg_world.objects[x].name)

    end
  end

  return cfg_session
end
