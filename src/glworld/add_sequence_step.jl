# ===========================================================
#  Copyright (C) 2022 Gerda Wyssen, Daniel Fitze and Matthias Ertl, University of Bern,
#  matthias.ertl@unibe.ch
#  gerda.wyssen@unibe.ch 
#  daniel.fitze@unibe.ch 
# 
#  This file is part of mpc.
# 
#  mpc is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
# 
#  mpc is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with mpc. If not, see <http://www.gnu.org/licenses/>.
#  ===========================================================
function add_sequence_step(cfg_session::session_def, seq::displacement)
  mg = @sprintf("%s,MOV,%f,%f,%f,%s,%s,%s,%f,%f,%f", 
                seq.name, 
                seq.object_id,
                seq.start_time, 
                seq.end_time,
                seq.abs_or_rel,
                seq.spatial_interp,
                seq.time_interp,
                seq.position.x,
                seq.position.y,
                seq.position.z)


  mpc.sendmsg(cfg_session, 
              mpc.AV_GLW_SEQ_ADD_STEP,
              mg)

  mpc_session, recv = mpc.srv_resp(cfg_session, 
                                   mpc.MSG_AV_GLW_ACK, 
                                   mpc.MSG_AV_GLW_NAK, 
                                   "[  Transmitted sequence step MOV  ]",
                                   "[  Something went wrong  ]")

  return cfg_session
end

function add_sequence_step(cfg_session::session_def, seq::rotation)
  mg = @sprintf("%s,ROT,%f,%f,%f,%s,%s,%s,%f,%f,%f", 
                seq.name, 
                seq.object_id,
                seq.start_time, 
                seq.end_time,
                seq.abs_or_rel,
                seq.spatial_interp,
                seq.time_interp,
                seq.orientation.x,
                seq.orientation.y,
                seq.orientation.z)


  mpc.sendmsg(cfg_session, 
              mpc.AV_GLW_SEQ_ADD_STEP,
              mg)

  mpc_session, recv = mpc.srv_resp(cfg_session, 
                                   mpc.MSG_AV_GLW_ACK, 
                                   mpc.MSG_AV_GLW_NAK, 
                                   "[  Transmitted sequence step ROT  ]",
                                   "[  Something went wrong  ]")

  return cfg_session
end

function add_sequence_step(cfg_session::session_def, seq)
  mg = @sprintf("%s,VIS,%f,%f,%d", 
                seq.name, 
                seq.object_id,
                seq.start_time, 
                seq.visible)

  mpc.sendmsg(cfg_session, 
              mpc.AV_GLW_SEQ_ADD_STEP,
              mg)

  mpc_session, recv = mpc.srv_resp(cfg_session, 
                                   mpc.MSG_AV_GLW_ACK, 
                                   mpc.MSG_AV_GLW_NAK, 
                                   "[  Transmitted sequence step VIS  ]",
                                   "[  Something went wrong  ]")

  return cfg_session
end

function add_sequence_step(cfg_session::session_def, seq::set_camera)
  mg = @sprintf("%s,CAM,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f",
                seq.name, 
                seq.object_id,
                seq.start_time, 
                seq.camera.pos.x,
                seq.camera.pos.y,
                seq.camera.pos.z,
                seq.camera.direction.x,
                seq.camera.direction.y,
                seq.camera.direction.z,
                seq.camera.rotation,
                seq.camera.fov)

  mpc.sendmsg(cfg_session, 
              mpc.AV_GLW_SEQ_ADD_STEP,
              mg)

  mpc_session, recv = mpc.srv_resp(cfg_session, 
                                   mpc.MSG_AV_GLW_ACK, 
                                   mpc.MSG_AV_GLW_NAK, 
                                   "[  Transmitted sequence step CAM  ]",
                                   "[  Something went wrong  ]")

  return cfg_session
end

function add_sequence_step(cfg_session::session_def, seq::Union{set_image, set_movie})
  mg = @sprintf("%s,SDS,%f,%f,%s",
                seq.name, 
                seq.object_id,
                seq.start_time, 
                seq.file)

  mpc.sendmsg(cfg_session, 
              mpc.AV_GLW_SEQ_ADD_STEP,
              mg)

  mpc_session, recv = mpc.srv_resp(cfg_session, 
                                   mpc.MSG_AV_GLW_ACK, 
                                   mpc.MSG_AV_GLW_NAK, 
                                   "[  Transmitted sequence step SDS  ]",
                                   "[  Something went wrong  ]")

  return cfg_session
end

function add_sequence_step(cfg_session::session_def, seq::color)
  mg = @sprintf("RGB,SDS,%f,%f,%d,%d,%d",
                seq.object_id,
                seq.start_time, 
                seq.r,
                seq.g,
                seq.b)

  mpc.sendmsg(cfg_session, 
              mpc.AV_GLW_SEQ_ADD_STEP,
              mg)

  mpc_session, recv = mpc.srv_resp(cfg_session, 
                                   mpc.MSG_AV_GLW_ACK, 
                                   mpc.MSG_AV_GLW_NAK, 
                                   "[  Transmitted sequence step SDS  ]",
                                   "[  Something went wrong  ]")

  return cfg_session
end


