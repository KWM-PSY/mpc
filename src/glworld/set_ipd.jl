# ===========================================================
#  Copyright (C) 2022 Gerda Wyssen, Daniel Fitze and Matthias Ertl, University of Bern,
#  matthias.ertl@unibe.ch
#  gerda.wyssen@unibe.ch 
#  daniel.fitze@unibe.ch 
# 
#  This file is part of mpc.
# 
#  mpc is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
# 
#  mpc is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with mpc. If not, see <http://www.gnu.org/licenses/>.
#  ===========================================================
function set_ipd(cfg_session::session_def, name::String, dist::Float64, xoff::Float64)
  mg = @sprintf("%s,%f,%f", 
                name, 
                dist, 
                xoff)

  mpc.sendmsg(cfg_session, 
              mpc.MSG_AV_GLW_SET_IPD, 
              mg)

  mpc_session, recv = mpc.srv_resp(cfg_session, 
                                   mpc.MSG_AV_GLW_ACK, 
                                   mpc.MSG_AV_GLW_NAK, 
                                   "[  Set IPD to $dist $xoff  ]",
                                   "[  Something went wrong  ]")

  return cfg_session
end

function set_ipd(dist::192.0, xoff::100.0)
  mg = @sprintf("%f,%f", 
                dist, 
                xoff)

  mpc.sendmsg(cfg_session, 
              mpc.MSG_FEEDBACK_IPD, 
              mg)

  return cfg_session
end
