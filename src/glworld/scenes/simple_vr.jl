# ===========================================================
#  Copyright (C) 2022 Gerda Wyssen, Daniel Fitze and Matthias Ertl, University of Bern,
#  matthias.ertl@unibe.ch
#  gerda.wyssen@unibe.ch 
#  daniel.fitze@unibe.ch 
# 
#  This file is part of mpc.
# 
#  mpc is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
# 
#  mpc is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with mpc. If not, see <http://www.gnu.org/licenses/>.
#  ===========================================================
function setup_simple_vr(cfg_session)
  # simple screens creates a world consiting of
  # 1) a flat screen presented on a pimaxx
  # 2) a light_source
  # 3) a camera
  # this scene can be used to present images and movies.


  # define world elements
  cfg_elem = [mpc.flat(name = "f1",
                       position = mpc.position3d(0.0, 0.0, 0.0),
                       resolution = mpc.resolution(500, 400),
                       dimension = mpc.dimension2d(5, 4)),
              mpc.light_source(),
              mpc.camera(screen_name = "vr1",
                         position = mpc.position3d(0.0, 0.0, 8.0)),
              mpc.camera(screen_name = "pc1",
                         position = mpc.position3d(0.0, 0.0, 8.0))
             ]

  cfg_world = mpc.glworld([mpc.vr(),
			   mpc.screen(position = mpc.position2d(1400,0),
                                      resolution = mpc.resolution(800,800))],
                          0,
                          cfg_elem)

  cfg_session = mpc.init_glworld(cfg_session,
                                 cfg_world)

  return cfg_session, cfg_world

end
