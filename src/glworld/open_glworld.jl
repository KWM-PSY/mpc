# ===========================================================
#  Copyright (C) 2022 Gerda Wyssen, Daniel Fitze and Matthias Ertl, University of Bern,
#  matthias.ertl@unibe.ch
#  gerda.wyssen@unibe.ch 
#  daniel.fitze@unibe.ch 
# 
#  This file is part of mpc.
# 
#  mpc is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
# 
#  mpc is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with mpc. If not, see <http://www.gnu.org/licenses/>.
#  ===========================================================
function open_glworld(cfg_session::session_def, cfg_glw::glworld)

  tmp = []
  
  for i = 1:length(cfg_glw.displays)
    if typeof(cfg_glw.displays[i]) == mpc.vr
      push!(tmp, cfg_glw.displays[i].name * "#" * cfg_glw.displays[i].type * "#" * string(cfg_glw.displays[i].motion_tracking))
    
    elseif typeof(cfg_glw.displays[i]) == mpc.screen
	    push!(tmp, cfg_glw.displays[i].name * "#" * string(cfg_glw.displays[i].position.x) * "#" * string(cfg_glw.displays[i].position.y) * "#" *  string(cfg_glw.displays[i].resolution.width) * "#" * string(cfg_glw.displays[i].resolution.height))

    else
      error("invalid output device! Please select vivepro, pimax or screen.")
    end
  end

  mg = join(tmp, "|")
  mg = @sprintf("%s,%d", 
                mg, 
                cfg_glw.dev_state)

  mpc.sendmsg(cfg_session, mpc.MSG_AV_OPEN_GLW, mg)
  mpc_session, recv = mpc.srv_resp(cfg_session, 
                                   mpc.MSG_AV_GLW_ACK, 
                                   mpc.MSG_AV_GLW_NAK, 
                                   "[  Opend GLworld successfully  ]",
                                   "[  Something went wrong  ]")

  return cfg_session
end
