# ===========================================================
#  Copyright (C) 2022 Gerda Wyssen, Daniel Fitze and Matthias Ertl, University of Bern,
#  matthias.ertl@unibe.ch
#  gerda.wyssen@unibe.ch 
#  daniel.fitze@unibe.ch 
# 
#  This file is part of mpc.
# 
#  mpc is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
# 
#  mpc is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with mpc. If not, see <http://www.gnu.org/licenses/>.
#  ===========================================================
function set_fov_glworld(cfg_session::session_def, cfg_camera::camera)

  mg = @sprintf("%s,%f,%f,%f", 
                cfg_camera.screen_name, 
                cfg_camera.fov, 
                cfg_camera.near, 
                cfg_camera.far)

  mpc.sendmsg(cfg_session, 
              mpc.MSG_AV_GLW_SET_FOVY, 
              mg)
  
  cname = cfg_camera.screen_name
  mpc_session, recv = mpc.srv_resp(cfg_session, 
                                   mpc.MSG_AV_GLW_ACK, 
                                   mpc.MSG_AV_GLW_NAK, 
                                   "[  Set FOV for screen $cname  ]",
                                   "[  Something went wrong  ]")

  return cfg_session
end
