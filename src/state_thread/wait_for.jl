# ===========================================================
#  Copyright (C) 2022 Gerda Wyssen, Daniel Fitze, Matthias Ertl, University of Bern,
#  gerda.wyssen@unibe.ch, daniel.fitze@unibe.ch, matthias.ertl@unibe.ch
# 
#  This file is part of mpc.
# 
#  mpc is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
# 
#  mpc is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with mpc.  If not, see <http://www.gnu.org/licenses/>.
#  ===========================================================

function wait_for(;type = mpc.sts, parameter::String, msg_ack::Union{Bool, Int, String, Nothing}, msg_nak::Union{Bool, Int, String, Nothing}, out_ack::Union{String, Nothing} = nothing, out_nak::Union{String, Nothing} = nothing, timeout::Float64 = 60.0, print_timeout::Bool = false)
    
    msg_type = "Status"

    if !isdefined(type, Symbol(parameter))
        mpc.print_msg("Warning", "waiting for invalid parameter")
    end

    t = time()
    while time() < t + timeout 
        if print_timeout
            mpc.print_timeout("[Timeout $(parameter)", round(timeout - (time() - t), digits = 1))
        end

        val = getfield(type, Symbol(parameter))
        sleep(0.05)
        if val == msg_ack 
            if mpc.cfg.rc.disp_info && !isnothing(out_ack)  
                mpc.print_msg(msg_type, out_ack) 
            end
            break
        elseif val == msg_nak
            if mpc.cfg.rc.disp_info && !isnothing(out_nak)
                mpc.print_msg("Warning", "$(out_nak) | $(mpc.sts.err[end,:])") 
            end
            break
        end
    end
    if time() > t + timeout && mpc.cfg.rc.disp_info 
        mpc.print_msg("Timeout", "waiting for $parameter ended. ]")
    end
end

#function wait_for_btn(;btn::String, timeout::Int64, print_timeout::Bool = true)
#    mpc.sts.input_state.listen_to_btn = true
#
#    t = time()
#    while time() < t + timeout 
#        if print_timeout
#            mpc.print_timeout("[Timeout $(btn)", 
#                              round(timeout - (time() - t), 
#                                    digits = 1))
#        end
#        
#        if !isnothing(mpc.sts.input_state.btn)
#            if mpc.sts.input_state.btn[end] == btn
#                break
#            end
#        end
#    end     
#    mpc.sts.input_state.btn = nothing
#    mpc.sts.input_state.listen_to_btn = false
#end


function wait_for_btn(;btn::Array{String}, timeout::Float64, print_timeout::Bool = true)
    mpc.sts.input_state.listen_to_btn = true
    
#    while ~mpc.sts.input_state.listen_to_btn
#        sleep(0.001)
#    end

    btn_name = [""]
    btn_time = [0.0]
    if timeout > 0.0
       t = time()
       while time() < t + timeout
           if print_timeout
                mpc.print_timeout("[Timeout $(btn)",
                                  round(timeout - (time() - t),
                                        digits = 1))
            end

            if !isnothing(mpc.sts.input_state.btn)
                if ~isempty(findall(x -> x == mpc.sts.input_state.btn[end], btn))
                    btn_name = mpc.sts.input_state.btn
                    btn_time = mpc.sts.input_state.btn_time
                    mpc.sts.input_state.btn = nothing
                    mpc.sts.input_state.listen_to_btn = false
                    return btn_name, time() - t
                end
            end
        end
    elseif timeout <= 0.0
        while true
            if !isnothing(mpc.sts.input_state.btn)
                if ~isempty(findall(x -> x == mpc.sts.input_state.btn[end], btn))
                    println("found $(mpc.sts.input_state.btn[end])")
                    btn_name = mpc.sts.input_state.btn
                    mpc.sts.input_state.btn = nothing
                    mpc.sts.input_state.listen_to_btn = false
                    return btn_name, time() - t 
                end
            end
        end
    end
    return ["no resp"], [-999]
end
