# ===========================================================
#  Copyright (C) 2022 Gerda Wyssen, Daniel Fitze, Matthias Ertl, University of Bern,
#  gerda.wyssen@unibe.ch, daniel.fitze@unibe.ch, matthias.ertl@unibe.ch
# 
#  This file is part of mpc.
# 
#  mpc is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
# 
#  mpc is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with mpc.  If not, see <http://www.gnu.org/licenses/>.
#  ===========================================================

function update_status(packet)
    sts.master_state = parse(Int, packet[3][1])
    sts.srv_state = parse(Int, packet[3][2])
    sts.pltf_state = tryparse(Int, packet[3][3])
    #println("master: ", sts.master_state, " | srv: ", sts.srv_state, " | pltf: ", sts.pltf_state)
    sts.current_dof.lateral = parse(Float64, packet[3][4])
    sts.current_dof.heave = parse(Float64, packet[3][5])
    sts.current_dof.surge = parse(Float64, packet[3][6])
    sts.current_dof.yaw = rad2deg(parse(Float64, packet[3][7]))
    sts.current_dof.pitch = rad2deg(parse(Float64, packet[3][8]))
    sts.current_dof.roll = rad2deg(parse(Float64, packet[3][9]))
end
