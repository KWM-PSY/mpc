function load_pltf_const(lab_config::LAB_CONFIG)
    global MOOGSKT_C_MODULE2 = lab_config.path_libmoogskt
    if  lab_config.pltf_model == "Moog_1"
        # MACHINE 1 (upstairs)
        global PLTF_STATE_POWERUP=1
        global PLTF_STATE_IDLE=1
        global PLTF_STATE_STBY=2
        global PLTF_STATE_ENGAGED=3
        global PLTF_STATE_PARKING=7
        global PLTF_STATE_FAULT1=8
        global PLTF_STATE_FAULT2=9
        global PLTF_STATE_FAULT3=10
        global PLTF_STATE_DISABLED=11
        global PLTF_STATE_INHIBITED=12

    elseif lab_config.pltf_model == "Moog_2"
        # MACHINE 2 (downstairs)
        global  PLTF2_STATE_OFF = 0
        global  PLTF2_STATE_WAIT_FOR_CONSENT = 1
        global  PLTF2_STATE_OPENING_BRIDGE = 2
        global  PLTF2_STATE_CLOSING_BRIDGE = 3
        global  PLTF2_STATE_STANDBY = 4
        global  PLTF2_STATE_PREPARING = 5
        global  PLTF2_STATE_BUFFER_TEST = 6
        global  PLTF2_STATE_INITIALIZING = 7
        global  PLTF2_STATE_READY_FOR_TRAIN = 8
        global  PLTF2_STATE_ENGAGED = 9
        global  PLTF2_STATE_HOLDING = 10
        global  PLTF2_STATE_DISENGAGING = 11
        global  PLTF2_STATE_FORCED_DISENGAGING = 12
        global  PLTF2_STATE_ABORTING = 13
        global  PLTF2_STATE_FAULT = 14
        global  PLTF2_STATE_RESET = 15
        global  PLTF2_STATE_LAST = 16

    elseif lab_config.pltf_model == "Yaw_2"
        global PLTFYAW_STATE_OFF=0
        global PLTFYAW_STATE_WAITCONNECT=1
        global PLTFYAW_STATE_CONNECTED=2
        global PLTFYAW_STATE_STARTED=3
        global PLTFYAW_STATE_FAULTY=4
    end
end

