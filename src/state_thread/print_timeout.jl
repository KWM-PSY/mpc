# ===========================================================
#  Copyright (C) 2022 Gerda Wyssen, Daniel Fitze, Matthias Ertl, University of Bern,
#  gerda.wyssen@unibe.ch, daniel.fitze@unibe.ch, matthias.ertl@unibe.ch
# 
#  This file is part of mpc.
# 
#  mpc is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
# 
#  mpc is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with mpc.  If not, see <http://www.gnu.org/licenses/>.
#  ===========================================================

function print_timeout(str, value)

    types = ["Status", "Topic", "Warning", "Timeout btn:", "Unknown"]
    length_max = maximum(length.(types))        
    gap_msg = length_max - length("Timeout btn:") + 1 
    gap_end = 20 - length(value) 


    printstyled("\u001b[1000DMPC: ", color = :light_black, bold = true)
    printstyled("[ Timeout btn:", color = :yellow)
    for i in 1:gap_msg print(" ") end
    print(" $value")
    for i in 1:gap_end print(" ") end
    printstyled("]\r", color = :yellow)
end

#mpc.print_timeout("[Timeout $(parameter): $(timeout - (time() + t))")

