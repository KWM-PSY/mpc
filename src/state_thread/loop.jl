# ===========================================================
#  Copyright (C) 2022 Gerda Wyssen, Daniel Fitze, Matthias Ertl, University of Bern,
#  gerda.wyssen@unibe.ch, daniel.fitze@unibe.ch, matthias.ertl@unibe.ch
# 
#  This file is part of mpc.
# 
#  mpc is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
# 
#  mpc is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with mpc.  If not, see <http://www.gnu.org/licenses/>.
#  ===========================================================

function loop()

    if !isdefined(mpc, :sts) error("Port not open! loop() should not be called on its own.") end
    # IT WOULD BE NICE TO BE ABLE LOG IN REAL TIME
    # ---------------------------------------------
    # https://github.com/JuliaLang/julia/issues/34363
    # https://www.juliabloggers.com/capturing-output-in-julia/ => capture erroras
    # - the attempt bellow does not work synchronously
    #file = open("/tmp/log", "a") #JL_O_APPEND | JL_O_SYNC)
    #println("before")
    #redirect_stdout(file)
    #redirect_stderr(file)
    #println("after")

    while sts.stop_loop == false
        recv = mpc.get_packet(sts.udp_port)

        if recv != nothing
# ============================================
# State Msg
            if recv[2] == MSG_STATE
                update_status(recv)
                recv = nothing
# ============================================
# Login & Engage
            elseif recv[2] == MSG_LOGIN_ACK
                mpc.sts.session_tag = recv[3][1]
                mpc.sts.login_state = MSG_LOGIN_ACK
                recv = nothing
            elseif recv[2] == MSG_LOGIN_NAK
                mpc.sts.login_state = MSG_LOGIN_NAK
                mpc.sts.err = vcat(mpc.sts.err, [recv[2] join(recv[3])])
                recv = nothing
            elseif recv[2] == MSG_ENGAGE_ACK
                mpc.sts.engage_state = MSG_ENGAGE_ACK
                recv = nothing
            elseif recv[2] == MSG_ENGAGE_NAK
                mpc.sts.engage_state = MSG_ENGAGE_NAK
                mpc.sts.err = vcat(mpc.sts.err, [recv[2] join(recv[3])])
                recv = nothing
            elseif recv[2] == MSG_ENGAGE_DONE
                mpc.sts.engage_state = MSG_ENGAGE_DONE
                recv = nothing

# ============================================
# IPD
            elseif recv[2] == MSG_AV_GLSTILLS_IPD_VAL
                if !isnothing(mpc.sts.input_state.ipd_state)
                    mpc.sts.input_state.ipd_state.ipd_val = parse(Int, recv[3][2]) #
                end
                recv = nothing
# ============================================
# IMG
            elseif recv[2] == MSG_AV_GLW_ACK
                mpc.sts.output_state.img_state = MSG_AV_GLW_ACK
                if recv[3][1] == "793" # disp_img() workes
                    mpc.sts.output_state.img_state = recv[3][3] # screen info is available but not considered
                end
                recv = nothing
            elseif recv[2] == MSG_AV_GLSTILLS_SERVER_READY
                mpc.sts.output_state.img_state = MSG_AV_GLSTILLS_SERVER_READY
                recv = nothing
            elseif recv[2] == MSG_AV_GLW_NAK
                mpc.sts.output_state.img_state = MSG_AV_GLW_NAK
                mpc.sts.err = vcat(mpc.sts.err, [recv[2] join(recv[3])])
                recv = nothing
# ============================================
# Audio
            elseif recv[2] == MSG_AV_AUDIO_ACK
                mpc.sts.output_state.audio_state = MSG_AV_AUDIO_ACK
                recv = nothing
            elseif recv[2] == MSG_AV_AUDIO_NAK
                mpc.sts.output_state.audio_state = MSG_AV_AUDIO_NAK
                mpc.sts.err = vcat(mpc.sts.err, [recv[2] join(recv[3])])
                recv = nothing
            elseif recv[2] == MSG_AV_AUDIO_SAMPLE_ENDED
                mpc.sts.output_state.audio_state = MSG_AV_AUDIO_SAMPLE_ENDED
                recv = nothing
# ============================================
# HW Input
            elseif recv[2] == MSG_HW_INPUT_ACK
                mpc.sts.input_state.gamepad_state = MSG_HW_INPUT_ACK
                recv = nothing
            elseif recv[2] == MSG_HW_INPUT_NAK
                mpc.sts.input_state.gamepad_state = MSG_HW_INPUT_NAK
                mpc.sts.err = vcat(mpc.sts.err, [recv[2] join(recv[3])])
                recv = nothing
            elseif recv[2] == MSG_HW_INPUT_EVENT
                @show recv[3]
                if mpc.sts.input_state.listen_to_btn # record only when listening
                    if recv[3][5] == "1" # record btn presses & discard btn releases
                        if mpc.sts.input_state.btn == nothing
                            mpc.sts.input_state.btn = [recv[3][4]]
                            mpc.sts.input_state.btn_time = [parse(Int, recv[3][1])]
                            mpc.sts.input_state.btn_pressed = true
                        else
                            push!(mpc.sts.input_state.btn, recv[3][4])
                            push!(mpc.sts.input_state.btn_time, parse(Int, recv[3][1]))
                        end

                    end
                end
                recv = nothing
# ============================================
# BTN Input (These are the analog buttons)
            elseif recv[2] == MSG_DPIN_CHANGE 
                if mpc.sts.input_state.listen_to_btn  # record only when listening
                    if recv[3][2] == "1"  # record btn presses & discard btn releases
                        if mpc.sts.input_state.btn == nothing
                            mpc.sts.input_state.btn = [recv[3][1]]
                            mpc.sts.input_state.btn_time = [parse(Int, recv[3][3])]
                            mpc.sts.input_state.btn_pressed = true
                        else
                            push!(mpc.sts.input_state.btn, recv[3][1])
                            push!(mpc.sts.input_state.btn_time, parse(Int, recv[3][3]))
                        end
                    end
                end
                recv = nothing
# ============================================
# Short Sequence
            elseif recv[2] == MSG_SHORT_SEQ_AT_HEIGHT
                mpc.sts.output_state.motion_state = MSG_SHORT_SEQ_AT_HEIGHT
                recv = nothing
            elseif recv[2] == MSG_SHORT_SEQ_ACK
                mpc.sts.output_state.motion_state = MSG_SHORT_SEQ_ACK
                mpc.sts.output_state.motion_start = recv[1]
                recv = nothing
            elseif recv[2] == MSG_SHORT_SEQ_NAK
                mpc.sts.output_state.motion_state = MSG_SHORT_SEQ_NAK
                mpc.sts.err = vcat(mpc.sts.err, [recv[2] join(recv[3])])
                recv = nothing
            elseif recv[2] == MSG_SHORT_SEQ_DONE
                mpc.sts.output_state.motion_state = MSG_SHORT_SEQ_DONE
                mpc.sts.output_state.motion_end = recv[1]
                recv = nothing
# ============================================
# Immediate Sequence
            elseif recv[2] == MSG_IMMEDIATE_ACK
                mpc.print_msg("Status", "Immediate ack")
                recv = nothing
# ============================================
# Logout & Park
            elseif recv[2] == MSG_LOGOUT_ACK
                mpc.sts.logout_state = MSG_LOGOUT_ACK
                recv = nothing
            elseif recv[2] == MSG_LOGOUT_NAK
                mpc.sts.logout_state = MSG_LOGOUT_NAK
                mpc.sts.err = vcat(mpc.sts.err, [recv[2] join(recv[3])])
                recv = nothing
            elseif recv[2] == MSG_PARK_ACK
                mpc.sts.park_state = MSG_PARK_ACK
                recv = nothing
            elseif recv[2] == MSG_PARK_NAK
                mpc.sts.park_state = MSG_PARK_NAK
                mpc.sts.err = vcat(mpc.sts.err, [recv[2] join(recv[3])])
                recv = nothing
            elseif recv[2] == MSG_PARK_DONE
                mpc.sts.park_state = MSG_PARK_DONE
                recv = nothing
# ============================================
# Feedback (Special Topic)
            elseif recv[2] == MSG_FEEDBACK_ACK
                println("[Acknowledged]")
                recv = nothing
                
# ============================================
# Topic
            elseif recv[2] == MSG_TOPIC
                @show recv
                mpc.sts.topic = recv[3][1]
                recv = nothing
# ============================================
# Feedback (special topic) 
            elseif recv[2] == MSG_FEEDBACK_ACK
                recv = nothing

# ============================================
# Timeout
            elseif recv[2] == MSG_IDLE_TIMEOUT
                mpc.print_msg("Warning", "Timeout reached!")
                sts.stop_loop == true
                recv = nothing
            end
        end

        if recv == nothing
            sleep(0.001)
# ============================================
# Unprocessed Msg
        else
            mpc.print_msg("Warning", "Unprocessesd MSG $recv[2]")
        end
    end
    # close session
    println("Bye")
    mpc.close(sts.udp_port)

end

