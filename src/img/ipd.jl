function get_ipd(screen)
    mpc.sendmsg(mpc.MSG_AV_GLSTILLS_GET_IPD, @sprintf("%s", screen))
end

function set_ipd(screen, ipd)
    mpc.sendmsg(mpc.MSG_AV_GLSTILLS_SET_IPD, @sprintf("%s,%s", screen, ipd))
end

@kwdef mutable struct ipd_setting
    ipd_val::Union{Int, Nothing} = nothing
    resp::Union{String, Nothing} = nothing
    done::Bool = false
end

function adjust_ipd(screen)
    fix_cross = mpc.img_stim(screen = screen,
                        name = "fixcross",
                        exit_on = "ack")
    black = mpc.img_stim(screen = screen,
                        name = "NONE",
                        exit_on = "ack")
    mpc.execute_event(fix_cross)

    adjust = 25
    mpc.sts.input_state.listen_to_btn = true
    mpc.sts.input_state.ipd_state = mpc.ipd_setting()  
    while mpc.sts.input_state.ipd_state.done == false
        sleep(0.1)
        mpc.get_ipd(screen)
        if mpc.sts.input_state.btn != nothing
            if mpc.sts.input_state.btn[end] == "fl1"
                mpc.set_ipd(screen, mpc.sts.input_state.ipd_state.ipd_val + (-1 * adjust))
                mpc.execute_event(fix_cross)
                mpc.sts.input_state.btn = nothing
            elseif mpc.sts.input_state.btn[end] == "fr1"
                mpc.set_ipd(screen, mpc.sts.input_state.ipd_state.ipd_val + (1 * adjust))
                mpc.execute_event(fix_cross)
                mpc.sts.input_state.btn = nothing
            elseif mpc.sts.input_state.btn[end] == "a"
                mpc.sts.input_state.ipd_state.done = true
                mpc.execute_event(black)
                mpc.sts.input_state.btn = nothing
            end
        end
    end
    mpc.sts.input_state.listen_to_btn = true
end




