# ===========================================================
#  Copyright (C) 2022 Gerda Wyssen, Daniel Fitze, Matthias Ertl, University of Bern,
#  gerda.wyssen@unibe.ch, daniel.fitze@unibe.ch, matthias.ertl@unibe.ch
# 
#  This file is part of mpc.
# 
#  mpc is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
# 
#  mpc is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with mpc.  If not, see <http://www.gnu.org/licenses/>.
#  ===========================================================

function prepare_disp_msg(screen::framed)
    msg = @sprintf("%s#%s#%d#%d#%d#%d", 
                   screen.name, 
                   screen.type, 
                   screen.top_left_x_coord, 
                   screen.top_left_y_coord, 
                   screen.upper_margin, 
                   screen.lower_margin)
    return msg
end

function prepare_disp_msg(screen::fullscreen)
    msg = @sprintf("%s#%s#%d#%d#%d#%d", 
                   screen.name, 
                   screen.type, 
                   screen.left_margin, 
                   screen.right_margin, 
                   screen.upper_margin, 
                   screen.lower_margin)
    return msg
end

