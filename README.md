mpc is a client application for the motion platform control software PlatformCommander [(Ertl et al., 2022)] (https://doi.org/10.1016/j.softx.2021.100945) developed by the Department of Psychology of the University of Bern.
mpc already implements most functions provided but most of them are untested. **mpc is work in progress** and will be subject to changes likely breaking working code. Backwards comaptibility is not a current goal. We are currently conducting the first experiments using mpc.

The latest stable branch is V0-0-1. 
We are currently working on a new major version V1-0-0, which should be available in summer 2024.

The [manual](10.5281/zenodo.574) of PlatformCommander describes the interface and some demo videos can be found elsewhere https://tube.switch.ch/channels/Zn0XXPs2tt.

mpc is a replacment for [MoogCom](https://gitlab.com/KWM-PSY/moogcom), an older client applications.
We highly recommend not to implement new experiments using MoogCom, since it will not be maintained anymore.

# Coding Standards For Contributors
- All units are in mm or degree
- The order of dimensions in arrays is: lateral, heave, surge, yaw, pitch, roll, [duration]

## Structs
- Each struct needs to be accompanied by a test script.
- A structs test script runs all test of function using the said script
```
  include("path/to/test/function_1/using/struct")
  include("path/to/test/function_2/using/struct")
  include("path/to/test/function_3/using/struct")
```
## Functions

## Todo

## Known Issues
- Timing of pulses sent via LPT is poor

## Stats (18.12.2022)
| language | files | lines blank | lines comment | lines code |
| ------ | ------ | ------ | ------ | ------ |
| Julia | 37 | 968 | 1313 | 3859 |
| Markdown | 1 | 11 | 0 | 47 |
| TOML | 1 | 1 | 0 | 12 |
| Sum | 39 | 980 | 1313 | 3918 |


