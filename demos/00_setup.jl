using mpc
#                
# LAB: set condiguration
lab = mpc.unibe_d369(pltf_model = "Moog_1",
                     sampling_freq = 60,
                     max_acc_allowed = 0.02,
                     client_ip = "192.168.1.212", 
                     server_ip = "192.168.1.200",
                     server_outport = 1860,
                     server_inport = 1966,
                     path_libmoogskt = "/home/kwm/julia/MOO-c-interface/c/libmoogskt.so",
                     disp_info = true,
                     path_data = "../data/",
                     log_resolution = 100,
                     mailto = "",
                     topic = "master")
#lab = mpc.unibe_d269(pltf_model = "Moog_2",
#                     sampling_freq = 200,
#                     max_acc_allowed = 300,
#                     client_ip = "10.33.1.50", 
#                     server_ip = "10.33.1.1",
#                     server_outport = 1860,
#                     server_inport = 1966,
#                     #path_libmoogskt = "/home/",
#                     disp_info = true,
#                     path_data = "../data/",
#                     log_resolution = 100,
#                     mailto = "",
#                     topic = "master")
# OUTPUT: define output-modalities to be used during the session
out = mpc.output(motion = mpc.short(actuator_mode = "D",
                                    start_height = 670),
                 audio = mpc.audio_srv(audio_dir = "old", 
                                       nb_channel = 2))
# INPUT: define input-modalities to be used during the session
inp = mpc.input()


# START SESSION
mpc.start_session(lab, inp, out)

# END SESSION
mpc.end_session(get_session_data = false)


