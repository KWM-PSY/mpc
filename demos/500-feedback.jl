using mpc

sess = mpc.feedback(active_joystick = "a",
                    axes_spec = "#p|x|-0.174533|0.174533#p|z|-0.174533|0.174533",
                    delay = 0.0,
                    smoothing_type = "E#",
                    smoothing_duration = 0.01,
                    buttons = [])

println(sess)

mpc.start_session(mpc.unibe_d269(client_ip = "10.33.1.52",
                                 path_libmoogskt = "/media/docer/Data/GIT/MOO-c-interface/c/libmoogskt2.so"),
                  mpc.input(),
                  mpc.output(motion = sess))



sleep(10)

mpc.end_session(get_session_data = false)


