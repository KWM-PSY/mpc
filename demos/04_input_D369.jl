using mpc
# prepare
disp = [mpc.framed(name = "disp",
                   top_left_x_coord = 1500,
                   top_left_y_coord = 0,
                   upper_margin = 640,
                   lower_margin = 480)]
start_screen = mpc.img_stim(screen = "disp",
                            name = "start_img",
                            exit_on = "ack")
black = mpc.img_stim(screen = "disp",
                     name = "NONE",
                     exit_on = "ack")
function start_seq(;button::String)
    mpc.execute_event(start_screen)
    mpc.wait_for_btn(btn = button,
                     timeout = 20,
                     print_timeout = true)
    mpc.execute_event(black)
    sleep(2)
end


# BUTTONS ----------------------
#mpc.start_session(mpc.unibe_d369new(topic="pl2"),
#                  mpc.input(btn = mpc.btn(btn_labels = ["hello", "world"])),
#                  mpc.output(motion = mpc.short(start_height = 670),
#                             img = mpc.img_srv(img_dir = "old",
#                                               displays = disp)))
#start_seq(button = "world")
#mpc.end_session(get_session_data = false)

# GAMEPAD ----------------------
mpc.start_session(mpc.unibe_d369new(topic="pl2"),
                  mpc.input(gamepad = mpc.gamepad(btn_nb = 3,
                                                  btn_names = ["fl1", "fr1", "a"])),
                  mpc.output(motion = mpc.short(start_height = 670),
                             img = mpc.img_srv(img_dir = "old",
                                               displays = disp)))
start_seq(button = "a")
mpc.end_session(get_session_data = false)

# LIKERT -----------------------
mpc.start_session(mpc.unibe_d369new(topic="pl2"),
                  mpc.input(likert = mpc.likert()),
                  mpc.output(motion = mpc.short(start_height = 670),
                             img = mpc.img_srv(img_dir = "e_motion",
                                               displays = disp)))
resp = mpc.setup_likert(screen = "disp")
mpc.execute_event(resp)
mpc.end_session(get_session_data = false)
