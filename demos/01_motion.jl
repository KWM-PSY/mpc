using mpc
mpc.start_session(mpc.unibe_d269(),
                  mpc.input(),
                  mpc.output(motion = mpc.short(start_height = 670)))

#=
Todo:
- add examples with differing rotational centers
=#

# EXAMPLE: CHANGE ROTATION CENTER 
# =========================================================
r_center_1 = mpc.axis_param(axis = "roll",
                             param = mpc.sine_steps(freq_acc = 1.0, 
                                                    peak_vel = 10.0),
                             rot_center = mpc.position3d(0.0,0.0,0.0))
r_center_2 = mpc.axis_param(axis = "roll",
                             param = mpc.sine_steps(freq_acc = 1.0, 
                                                    peak_vel = 10.0),
                             rot_center = mpc.position3d(0.0,0.0,1000.0))

roll1 = mpc.prepare_motion(r_center_1,
                               exit_on = "motion",
                               reverse = false,
                               log = false)
#
roll1_rev = mpc.prepare_motion(r_center_1,
                               exit_on = "motion",
                               reverse = true,
                               log = false)
mpc.execute_event(roll1)
mpc.execute_event(roll1_rev)

roll2 = mpc.prepare_motion(r_center_2,
                           exit_on = "motion",
                           reverse = false,
                           log = false)
#
roll2_rev = mpc.prepare_motion(r_center_2,
                               exit_on = "motion",
                               reverse = true,
                               log = false)
mpc.execute_event(roll2)
mpc.execute_event(roll2_rev)


#
# SINE STEPS
# EXAMPLE WITH MOTION ALONG TWO AXIS
# =========================================================
start_no = mpc.axis_param(axis = "surge",
                          param = mpc.sine_steps(freq_acc = 1.0, 
                                                 peak_vel = 100.0))
start_ia = mpc.axis_param(axis = "lateral",
                          param = mpc.sine_steps(freq_acc = 1.0, 
                                                 peak_vel = 100.0))
param_ia1 = mpc.axis_param(axis = "lateral",
                           param = mpc.sine_steps(freq_acc = 1.0, 
                                                  peak_vel = -200.0))
param_ia2 = mpc.axis_param(axis = "lateral",
                           param = mpc.sine_steps(freq_acc = 1.0, 
                                                  peak_vel = 200.0))
param_no1 = mpc.axis_param(axis = "surge",
                           param = mpc.sine_steps(freq_acc = 1.0, 
                                                  peak_vel = -200.0))
param_no2 = mpc.axis_param(axis = "surge",
                           param = mpc.sine_steps(freq_acc = 1.0, 
                                                  peak_vel = 200.0))

# Example using current_dof 
start_pos = mpc.prepare_motion([start_no, start_ia],
                               exit_on = "motion",
                               reverse = false,
                               log = false)
mpc.execute_event(start_pos)
#
for i in 1:3
    dir1 = mpc.prepare_motion([param_no2, param_ia2],
                              start_dof = mpc.sts.current_dof,
                              exit_on = "motion",
                              reverse = false,
                              log = false)
    mpc.execute_event(dir1)
    #
    dir2 = mpc.prepare_motion([param_no1,param_ia1],
                              start_dof = mpc.sts.current_dof,
                              exit_on = "motion",
                              reverse = false,
                              log = false)
    mpc.execute_event(dir2)
end
#
center_pos = mpc.prepare_motion([start_no, start_ia],
                                exit_on = "motion",
                                reverse = true,
                                log = false)
mpc.execute_event(center_pos)

# Example using reverse
stim = mpc.prepare_motion(start_no,
                          exit_on = "motion",
                          reverse = false,
                          log = false)
#
stim_reverse = mpc.prepare_motion(start_no,
                                  exit_on = "motion",
                                  reverse = true,
                                  log = false)
#
for i in 1:3
    mpc.execute_event(stim)
    mpc.execute_event(stim_reverse)
end

# SINE STEPS
# EXAMPLE WITH MOTION ALONG ONE AXIS
# =========================================================
# Example using current_dof 
start_pos = mpc.prepare_motion(start_ia,
                               exit_on = "motion",
                               reverse = false,
                               log = false)
mpc.execute_event(start_pos)
#
for i in 1:3
    dir1 = mpc.prepare_motion(param_ia2,
                              start_dof = mpc.sts.current_dof,
                              exit_on = "motion",
                              reverse = false,
                              log = false)
    mpc.execute_event(dir1)
    #
    dir2 = mpc.prepare_motion(param_ia1,
                              start_dof = mpc.sts.current_dof,
                              exit_on = "motion",
                              reverse = false,
                              log = false)
    mpc.execute_event(dir2)
end
#
center_pos = mpc.prepare_motion(start_ia,
                                exit_on = "motion",
                                reverse = true,
                                log = false)
mpc.execute_event(center_pos)












# CONV SINE STEPS
conv_sine = mpc.prepare_motion(axis = "lateral",
                               step_param = mpc.conv_sine_steps(freq_acc = 1.0, 
                                                                peak_vel = 250.0,
                                                                cycles = 2,
                                                                kernel = "gaussian"),
                               exit_on = "motion",
                               reverse = false,
                               log = false)
# NOISE STEPS
noise = mpc.prepare_motion(axis = "lateral",
                           step_param = mpc.noise_steps(freq_acc = 1.0, 
                                                        noise = 0.5),
                           exit_on = "motion",
                           reverse = false,
                           log = false)
# LINEAR STEPS
linear = mpc.prepare_motion(axis = "lateral",
                            step_param = mpc.linear_steps(gradient = 20.0, 
                                                          duration = 2.0),
                            exit_on = "motion",
                            reverse = false,
                            log = false)
# TEMPLATE MOTION
center = mpc.motion_template(dof = mpc.pltf_position(heave = 670.0),
                             duration = 2.0,
                             law = "S",
                             log = false,
                             exit_on = "motion")


for motion in [sine, conv_sine, noise, linear]
    mpc.execute_event(motion)
    mpc.execute_event(center)
end

mpc.end_session(get_session_data = false)


