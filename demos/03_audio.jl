using mpc
mpc.start_session(mpc.unibe_d269(),
                  mpc.input(),
                  mpc.output(motion = mpc.short(start_height = 670),
                             audio = mpc.audio_srv(audio_dir = "old",
                                                   nb_channel = 3)))


# set alsa absolute volumes
mpc.set_volume(vol_out = 0.1, vol_in = 0.1)
mpc.set_volume(vol_out = 1.0, vol_in = 1.0)


# AUDIO STIMULI
white_noise = mpc.audio(name = "mesto211.wav",
                        exit_on = "immediate",
                        repetitions = 0,
                        chan = 0)
audio_stop = mpc.interrupt_audio(audio = white_noise,
                                exit_on = "immediate")
beep = mpc.audio(name = "Kap2.wav",
                 exit_on = "immediate",
                 repetitions = 0,
                 chan = 1)

mpc.execute_event(white_noise)

#sleep(1.5)

mpc.execute_event(beep)

mpc.execute_event(audio_stop)

# use mic

mpc.end_session(get_session_data = false)


