using mpc
disps = [
        mpc.framed(name = "window1",
                   top_left_x_coord = 1500,
                   top_left_y_coord = 0,
                   upper_margin = 640,
                   lower_margin = 480),
        mpc.framed(name = "window2",
                   top_left_x_coord = 1500,
                   top_left_y_coord = 800,
                   upper_margin = 640,
                   lower_margin = 480),
        mpc.fullscreen(name = "PIMAX",
                       left_margin = 60,
                       right_margin = 60,
                       lower_margin = 60,
                       upper_margin = 60,
                       type = "VISOR_PIMAX8"),
       ]

mpc.start_session(mpc.unibe_d269(),
                  mpc.input(gamepad = mpc.gamepad(btn_nb = 3,
                                                  btn_names = ["fl1", "fr1", "a"])),
                  mpc.output(motion = mpc.short(start_height = 670),
                             img = mpc.img_srv(img_dir = "old",
                                               displays = disps)))


# SET IPD
mpc.adjust_ipd("PIMAX")

# IMG STIMULI
img1 = mpc.img_stim(screen = "window1",
                    name = "b150",
                    exit_on = "ack")
img2 = mpc.img_stim(screen = "window2",
                    name = "b15",
                    exit_on = "ack")
img3 = mpc.img_stim(screen = "PIMAX",
                    name = "b157",
                    exit_on = "ack")
black1 = mpc.img_stim(screen = "window1",
                      name = "NONE",
                      exit_on = "ack")
black2 = mpc.img_stim(screen = "window2",
                      name = "NONE",
                      exit_on = "ack")
black3 = mpc.img_stim(screen = "PIMAX",
                      name = "NONE",
                      exit_on = "ack")

mpc.execute_event(img1)
sleep(2)
mpc.execute_event(img2)
sleep(2)
mpc.execute_event(img3)
sleep(2)
mpc.execute_event(black1)
sleep(2)
mpc.execute_event(black2)
sleep(2)
mpc.execute_event(black3)

mpc.end_session(get_session_data = false)


