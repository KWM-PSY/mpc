using mpc

disp = [mpc.framed(name = "disp",
                   top_left_x_coord = 1500,
                   top_left_y_coord = 0,
                   upper_margin = 640,
                   lower_margin = 480)]

mpc.start_session(mpc.unibe_d369new(topic = "pl2"),
                  mpc.input(likert = mpc.likert()),
                  mpc.output(motion = mpc.short(start_height = 670),
                             audio = mpc.audio_srv(audio_dir = "old",
                                                   nb_channel = 3),
                             img = mpc.img_srv(img_dir = "old",
                                               displays = disp)))

# EVENTS
white_noise = mpc.audio(name = "whitenoise_test",
                        exit_on = "immediate",
                        chan = 0)
audio_stop = mpc.interrupt_audio(audio = white_noise,
                                exit_on = "immediate")
beep = mpc.audio(name = "beep",
                 exit_on = "immediate",
                 chan = 1)
motion = mpc.motion_template(dof = mpc.pltf_position(heave = 670.0,
                                                     lateral = -150.0), 
                             duration = 2.0,
                             law = "S",
                             log = true,
                             exit_on = "immediate");
center = mpc.motion_template(dof = mpc.pltf_position(heave = 670.0),
                             duration = 2.0,
                             law = "S",
                             log = false,
                             exit_on = "immediate")
wait = mpc.wait(on = "btn")
btn_listen = mpc.btn_switch(listening = true)
btn_off = mpc.btn_switch(listening = false)
likert_response = mpc.setup_likert(screen = "disp")


# TRIAL
trial = mpc.trial_structure(event_time = [
                                          (btn_listen, 0.0),
                                          (beep, 0.0),
                                          (white_noise, 0.6),
                                          (motion, 0.6),
                                          (wait, 0.6),
                                          ##------------#
                                          (audio_stop, 0.0),
                                          (btn_off, 0.0),
                                          (beep, 5.0),
                                          (center, 5.0)
                                         ])

mpc.run_trial(trial)

# LOGOUT
mpc.end_session(get_session_data = false)


