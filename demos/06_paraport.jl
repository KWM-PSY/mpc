# Paraport server setup after every reboot
# ssh srv2
# cd /home/moog/c/master/sub/cutils/ext/cparp/emu 
# sudo insmod emu.ko

using mpc

disp = [mpc.framed(name = "disp",
                   top_left_x_coord = 1500,
                   top_left_y_coord = 0,
                   upper_margin = 640,
                   lower_margin = 480)]

mpc.start_session(mpc.unibe_d269(),
                  mpc.input(),
                  mpc.output(motion = mpc.short(start_height = 670),
                             img = mpc.img_srv(img_dir = "old",
                                               displays = disp)))

# put this into start_unit()
# init parport alsways after opening glstills
mpc.init_parport(port_nb = 2, 
                 pulse_length = 10000)


trig = mpc.trigger_visible(screen = "disp",
                           onset = 223) # onset = pinconfiguration to turn on

mpc.define_trigger(trig) # define trigger before each execution of event

# IMAGES
fix_cross = mpc.img_stim(screen = "disp",
                         name = "fixcross",
                         exit_on = "immediate")
img1 = mpc.img_stim(screen = "disp",
                    name = "b150",
                    exit_on = "immediate")
img2 = mpc.img_stim(screen = "disp",
                    name = "b153",
                    exit_on = "immediate")

# Motion
motion = mpc.motion_template(dof = mpc.pltf_position(heave = 670.0,
                                                     lateral = -150.0), 
                             duration = 2.0,
                             law = "S",
                             log = true,
                             exit_on = "motion");
center = mpc.motion_template(dof = mpc.pltf_position(heave = 670.0),
                             duration = 2.0,
                             law = "S",
                             log = false,
                             exit_on = "motion")


# TRIAL
mpc.execute_event(fix_cross)
mpc.execute_event(motion)
mpc.execute_event(center)

# LOGOUT
mpc.end_session(get_session_data = false)


